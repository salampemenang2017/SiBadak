@extends('layouts.blank-page-dinas')
@section('title')
Dinas - Riwayat Daftar Kegiatan smpn 2 sukaluyu
@stop
@section('page-name')
Riwayat Daftar Kegiatan smpn 2 sukaluyu
@stop
@section('sidecontent')
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
	<li class="nav-item has-treeview">
		<a href="#" class="nav-link">
			<i class="nav-icon fas fa-history"></i>
			<p class="text-white">
				Riwayat

				<i class="fas fa-angle-left right"></i>
			</p>
		</a>
		<ul class="nav nav-treeview">
			<li class="nav-item has-treeview">
				<a href="#" class="nav-link" id="button1">
					<i class="fas fa-angle-right nav-icon"></i>
					<p class="text-white">2017</p>
				</a>
			</li>
		</ul>
	</li>
</ul>
@stop
@section('content')
<div class="content-wrapper py-4">
	<!-- Main content -->
	<section class="content">
		<div class="container">
			<div class="card show1">
				<div class="card-header">
					<h5 class="card-title">SMPN 2 SUKALUYU Tahun 2017</h5>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="{{ route('dinas-riwayat-dak') }}" data-source-selector="#card-refresh-content"><i class="fas fa-sync-alt"></i></button>
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="container">
						<div class="row">
							<div class="col-lg-10">
								<div class="row">
									<div class="colsm">
										<i class="fas fa-info-circle"></i>  
									</div>
									<div class="col">
										<h6>Pembangunan lab komputer 1 kls</h6>
									</div>
								</div> 
							</div>
							<div class="col-lg-2">
								<small>
									<a href="{{ url('laporan-rancangan-anggaran-biaya-sekolah/smpn-2-sukaluyu') }}" class="stretched-link">(klik Informasi RAB)</a>
								</small>
							</div>
						</div>
						<div class="row mt-3">
							<div class="col-md-4">
								<div class="card" style="width: 18rem;" data-toggle="modal" data-target="#modal-lg">
									<a href="#">
										<img style="height: 230px;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSHTLdoD2KS-2nn_pj6NiuxR8EkEbgTnyP-JYoyDAbadOY3Qaf" class="card-img-top" id="foto1">
									</a>
									<div class="card-body">
										<div class="row">
											<div class="col-md-9">
												<small class="text-muted">Diunggah <strong>27 September 2017</strong></small> 
											</div>
											<div class="col-md-1">
												<span class="badge badge-danger">Sebelum</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card" style="width: 18rem;">
									<a href="#">
										<img style="height: 230px;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRQPjLkprhJik-rL2NTGybSjWPeVVcYZI3AtebJdBf7ye9gxgnp" class="card-img-top" id="foto2">
									</a>
									<div class="card-body">
										<div class="row">
											<div class="col-md-9">
												<small class="text-muted">Diunggah <strong>27 Oktober 2017</strong></small> 
											</div>
											<div class="col-md-1">
												<span class="badge badge-warning">Proses</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card" style="width: 18rem;">
									<a href="#">
										<img style="height: 230px;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTyF15nNTHjBvbJ0z53c2Bg3u-O1Su3oA77aowRaTj7aWlwVBv0" class="card-img-top" id="foto3">
									</a>
									<div class="card-body">
										<div class="row">
											<div class="col-md-9">
												<small class="text-muted">Diunggah <strong>23 Desember 2017</strong></small> 
											</div>
											<div class="col-md-1">
												<span class="badge badge-primary">Selesai</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="modal fade" id="modal-lg">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">
					pembangunan lab komputer 1 kls
					<span class="badge badge-danger">Sebelum</span>
				</h4>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Keluar</button>
			</div>
			<div class="modal-body">
				<div id="demo" class="carousel slide" data-ride="carousel">
					<ul class="carousel-indicators">
						<li data-target="#demo" data-slide-to="0" class="active"></li>
						<li data-target="#demo" data-slide-to="1"></li>
						<li data-target="#demo" data-slide-to="2"></li>
					</ul>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSHTLdoD2KS-2nn_pj6NiuxR8EkEbgTnyP-JYoyDAbadOY3Qaf" class="card-img-top" width="1100" height="500"> 
						</div>
						<div class="carousel-item">
							<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSHTLdoD2KS-2nn_pj6NiuxR8EkEbgTnyP-JYoyDAbadOY3Qaf" class="card-img-top" width="1100" height="500">
						</div>
						<div class="carousel-item">
							<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSHTLdoD2KS-2nn_pj6NiuxR8EkEbgTnyP-JYoyDAbadOY3Qaf" class="card-img-top" alt="New York" width="1100" height="500"> 
						</div>
					</div>
					<a class="carousel-control-prev" href="#demo" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#demo" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@stop	
@section('script-js')
<script type="text/javascript">
	// Hide and Show
	$(document).ready(function() {
		
		$(".show1").hide();
		$("#button1").click(function(){
			$(".show1").show();
		});
	});
</script>
@stop
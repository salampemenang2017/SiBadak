@extends('layouts.blank-page-dinas')

@section('title')
Dinas - Laporan Rancangan Anggaran Biaya : Kelas Baru Dua Ruangan 70X70
@stop

@section('page-name')
Rancangan Anggaran Biaya: Pembangunan Kelas Baru Dua Ruangan 70X70
@stop

@section('content')
<div class="content-wrapper py-4">
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-8">
					<div class="card card-primary card-outline p-2">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-2">
									<img src="{{ asset('assets/dist/img/nuris.png') }}" width="110" height="110" class="align-self-start" alt="Logo Sekolah">
								</div>
								<div class="col-lg-1">
									
								</div>
								<div class="col-lg-9">
									<h2>
										<strong>SMK NURUL ISLAM</strong>
									</h2>
									<p>
										Jl. Raya Cianjur Bandung Km. 09, Sukaluyu, Selajambe, Cianjur, Kabupaten Cianjur, Jawa Barat 43215
										<a href="#" class="text-dark mb-5" title="Lihat Lokasi dipeta" data-toggle="modal" data-target="#lokasi">
											<small>
												<sup>
													<i class="fas fa-search-location"></i> Lihat Lokasi
												</sup>
											</small>
										</a>
									</p>
								</div>
							</div>
						</div>        
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-primary card-outline p-2">
						<div class="card-header">
							<center>
								<strong>
									<h2>Total Anggaran</h2>
								</strong>
							</center>
						</div>
						<div class="card-body">
							<center>
								<h4>
									Rp. 1.425.000,-
								</h4>
							</center>
						</div>
					</div>
				</div>    
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<h3 class="card-title">
								Laporan RAB : Pembangunan Kelas Baru Dua Ruangan 70X70
							</h3>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><center>Tanggal</center></th>
										<th><center>Uraian Pekerjaan</center></th>
										<th><center>Volume</center></th>
										<th><center>Satuan</center></th>
										<th><center>Harga Satuan</center></th>
										<th><center>Jumlah Harga</center></th>
										<th><center>Faktur</center></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>27/10/2017 09:12:55</td>
										<td>Pekerjaan Pengukuran</td>
										<td>1</td>
										<td>Is</td>
										<td>Rp. 150.000,-</td>
										<td>Rp. 150.000,-</td>
										<td>
											<a href="#" title="Lihat Faktur" data-toggle="modal" data-target="#faktur1">
												<i class="fas fa-folder text-warning"></i>
											</a>
										</td>
									</tr>
									<tr>
										<td>30/10/2017 10:00:51</td>
										<td>Pekerjaan Pasang Pondasi Batu</td>
										<td>30</td>
										<td>m<sup><small>1</small></sup></td>
										<td>Rp. 5.000,-</td>
										<td>Rp. 150.000,-</td>
										<td>
											<a href="#" title="Lihat Faktur" data-toggle="modal" data-target="#faktur2">
												<i class="fas fa-folder text-warning"></i>
											</a>
										</td>
									</tr>
									<tr>
										<td>04/11/2017 08:34:55</td>
										<td>Pekerjaan Pasang Batu Bata</td>
										<td>35</td>
										<td>m<sup><small>2</small></sup></td>
										<td>Rp. 15.000,-</td>
										<td>Rp. 525.000,-</td>
										<td>
											<a href="#" title="Lihat Faktur" data-toggle="modal" data-target="#faktur3">
												<i class="fas fa-folder text-warning"></i>
											</a>
										</td>
									</tr>
									<tr>
										<td>15/11/2017 11:45:55</td>
										<td>Pembelian Semen</td>
										<td>12</td>
										<td>zak</td>
										<td>Rp. 50.000,-</td>
										<td>Rp. 600.000,-</td>
										<td>
											<a href="#" title="Lihat Faktur" data-toggle="modal" data-target="#faktur4">
												<i class="fas fa-folder text-warning"></i>
											</a>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr style="background: #45aaf2; color: #FFFFFF;">
									<td colspan="4"></td>
									<td><strong>TOTAL</strong></td>
									<td><strong>Rp. 1.425.000,-</strong></td>
									<td></td>
								</tr>
								</tfoot>
							</table>
						</div>          
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
{{-- Modal Lokasi Sekolah --}}
<div class="modal fade bd-example-modal-lg" id="lokasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Lokasi Sekolah</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="position: relative;overflow: hidden;padding-top: 56.25%;">
				<iframe style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;border: 0;" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJAyvRLV1TaC4RR7QfU1S2CLs&key=AIzaSyCJgu7nyrc6KwvXMkgZwAy7eQYxdNPoYwo" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

{{-- Modal Faktur1 --}}
<div class="modal fade" id="faktur1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Faktur Digital</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{ asset('/assets/dist/img/faktur_pengukuran.jpg') }}" class="img-thumbnail">
			</div>
		</div>
	</div>
</div>

{{-- Modal Faktur2 --}}
<div class="modal fade" id="faktur2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Faktur Digital</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{ asset('/assets/dist/img/faktur_pembangunan.jpg') }}" class="img-thumbnail">
			</div>
		</div>
	</div>
</div>

{{-- Modal Faktur3 --}}
<div class="modal fade" id="faktur3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Faktur Digital</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{ asset('/assets/dist/img/faktur_batubata.jpg') }}" class="img-thumbnail">
			</div>
		</div>
	</div>
</div>

{{-- Modal Faktur4 --}}
<div class="modal fade" id="faktur4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Faktur Digital</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{ asset('/assets/dist/img/faktur_semen.jpg') }}" class="img-thumbnail">
			</div>
		</div>
	</div>
</div>
@stop
@section('script-js')
<script>
	$(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
		});
	});
</script>
@stop
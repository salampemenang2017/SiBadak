@extends('layouts.blank-page-dinas')
@section('title')
Papan Informasi
@stop
@section('page-name')
Papan Informasi 
@stop
@section('content')
<div class="content-wrapper py-4">
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4">
          <div class="info-box">
            <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Operator Sekolah</strong></span>
              <span>1.200</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-check-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total pengajuan DAK diterima</span>
              <span class="info-box-number">Rp.9.000.229.999</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-bill-wave"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Pengajuan DAK</span>
              <span class="info-box-number">Rp.100.920.320.023</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>        
      </div>
      <!-- =========================================================== -->
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="info-box bg-gradient-info">
            <span class="info-box-icon"><i class="far fa-check-square"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pengajuan DAK</span>
              <span class="info-box-number">800</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                70% Mengajukan DAK
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="info-box bg-gradient-danger">
            <span class="info-box-icon"><i class="far fa-square"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Non Pengajuan</span>
              <span class="info-box-number">150</span>

              <div class="progress">
                <div class="progress-bar" style="width: 13%"></div>
              </div>
              <span class="progress-description">
                13% <small>Tidak Mengajukan DAK</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="info-box bg-gradient-info">
            <span class="info-box-icon"><i class="far fa-check-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Dapat DAK</span>
              <span class="info-box-number">800</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                70% Dapat DAK
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="info-box bg-gradient-danger">
            <span class="info-box-icon"><i class="far fa-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Tidak Dapat DAK</span>
              <span class="info-box-number">400</span>

              <div class="progress">
                <div class="progress-bar" style="width: 30%"></div>
              </div>
              <span class="progress-description">
                30% Tidak Dapat DAK
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-chart-pie"></i>
                Informasi sifat pengajuan DAK pie Chart
              </h3>
            </div>
            <div class="card-body">
             <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
           </div>
           <!-- /.card-body-->
         </div>
       </div>

       <div class="col-lg-6">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="far fa-chart-bar"></i>
              informasi pengajuan DAK Bar Chart
            </h3>
          </div>
          <div class="card-body">
            <div id="bar-chart" style="height: 250px;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="height: 100%;">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="far fa-chart-bar"></i>
              Informasi Maps
            </h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                <div class="maps">

                </div>
              </div>
              <div class="col-lg-4" style="display:block; height:500px; overflow:auto;">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="agrabinta">Kecamatan Agrabinta
                        <span class="badge badge-info float-right">20.43%</span>
                      </a>
                      <span class="product-description">
                        Rp. 50.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="bojongpicung">Kecamatan Bojongpicung
                        <span class="badge badge-info float-right">20.43%</span>
                      </a>
                      <span class="product-description">
                        Rp. 50.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="campaka">Kecamatan Campaka
                        <span class="badge badge-info float-right">10.35%</span>
                      </a>
                      <span class="product-description">
                        Rp. 8.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="campakamulya">Kecamatan Campaka Mulya
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="kec-cianjur">Kecamatan Cianjur
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cibeber">Kecamatan Cibeber
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cibinong">Kecamatan Cibinong
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cidaun">Kecamatan Cidaun
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cijati">Kecamatan Cijati
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cikadu">Kecamatan Cikadu
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cikalongkulon">Kecamatan Cikalong Kulon
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cilaku">Kecamatan Cilaku
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cipanas">Kecamatan Cipanas
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="ciranjang">Kecamatan Ciranjang
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="cugenang">Kecamatan Cugenang
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="gekbrong">Kecamatan Gekbrong
                        <span class="badge badge-info float-right">5.94%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.100.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="haurwangi">Kecamatan Haurwangi
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="kadupandak">Kecamatan Kadupandak
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="karangtengah">Kecamatan Karang Tengah
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="leles">Kecamatan Leles
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="mande">Kecamatan Mande
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="naringgul">Kecamatan Naringgul
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="pacet">Kecamatan Pacet
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="pagelaran">Kecamatan Pagelaran
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="pasirkuda">Kecamatan Pasir Kuda
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="sindangbarang">Kecamatan Sindang Barang
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="sukaluyu">Kecamatan Sukaluyu
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="sukanagara">Kecamatan Sukanagara
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="sukaresmi">Kecamatan Sukaresmi
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="takokak">Kecamatan Takokak
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="tanggeung">Kecamatan Tanggeung
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fas fa-poll-h" style="font-size: 50px;"></i>
                    </div>
                    <div class="product-info">
                      <a class="klik_menu" href="###" id="warungkondang">Kecamatan Warung Kondang
                        <span class="badge badge-info float-right">7.87%</span>
                      </a>
                      <span class="product-description">
                        Rp. 1.220.000
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section>
</div>
@stop


@section('script-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('.klik_menu').click(function(){
      var menu = $(this).attr('id');
      if(menu == "agrabinta"){
        $('.maps').load('{!! route('agrabinta') !!}');           
      }else if(menu == "bojongpicung"){
        $('.maps').load('{!! route('bojongpicung') !!}');            
      }else if(menu == "campaka"){
        $('.maps').load('{!! route('campaka') !!}');           
      }else if(menu == "kec-cianjur"){
        $('.maps').load('{!! route('kec-cianjur') !!}');           
      }else if(menu == "campakamulya"){
        $('.maps').load('{!! route('campakamulya') !!}');           
      }else if(menu == "cibeber"){
        $('.maps').load('{!! route('cibeber') !!}');           
      }else if(menu == "cibinong"){
        $('.maps').load('{!! route('cibinong') !!}');           
      }else if(menu == "cidaun"){
        $('.maps').load('{!! route('cidaun') !!}');           
      }else if(menu == "cijati"){
        $('.maps').load('{!! route('cijati') !!}');           
      }else if(menu == "cikadu"){
        $('.maps').load('{!! route('cikadu') !!}');           
      }else if(menu == "cikalongkulon"){
        $('.maps').load('{!! route('cikalongkulon') !!}');           
      }else if(menu == "cilaku"){
        $('.maps').load('{!! route('cilaku') !!}');           
      }else if(menu == "cipanas"){
        $('.maps').load('{!! route('cipanas') !!}');           
      }else if(menu == "ciranjang"){
        $('.maps').load('{!! route('ciranjang') !!}');           
      }else if(menu == "cugenang"){
        $('.maps').load('{!! route('cugenang') !!}');           
      }else if(menu == "gekbrong"){
        $('.maps').load('{!! route('gekbrong') !!}');           
      }else if(menu == "haurwangi"){
        $('.maps').load('{!! route('haurwangi') !!}');           
      }else if(menu == "kadupandak"){
        $('.maps').load('{!! route('kadupandak') !!}');           
      }else if(menu == "karangtengah"){
        $('.maps').load('{!! route('karangtengah') !!}');           
      }else if(menu == "leles"){
        $('.maps').load('{!! route('leles') !!}');           
      }else if(menu == "mande"){
        $('.maps').load('{!! route('mande') !!}');           
      }else if(menu == "naringgul"){
        $('.maps').load('{!! route('naringgul') !!}');           
      }else if(menu == "pacet"){
        $('.maps').load('{!! route('pacet') !!}');           
      }else if(menu == "pagelaran"){
        $('.maps').load('{!! route('pagelaran') !!}');           
      }else if(menu == "pasirkuda"){
        $('.maps').load('{!! route('pasirkuda') !!}');           
      }else if(menu == "sindangbarang"){
        $('.maps').load('{!! route('sindangbarang') !!}');           
      }else if(menu == "sukaluyu"){
        $('.maps').load('{!! route('sukaluyu') !!}');           
      }else if(menu == "sukanagara"){
        $('.maps').load('{!! route('sukanagara') !!}');           
      }else if(menu == "sukaresmi"){
        $('.maps').load('{!! route('sukaresmi') !!}');           
      }else if(menu == "takokak"){
        $('.maps').load('{!! route('takokak') !!}');           
      }else if(menu == "tanggeung"){
        $('.maps').load('{!! route('tanggeung') !!}');           
      }else if(menu == "warungkondang"){
        $('.maps').load('{!! route('warungkondang') !!}');           
      }
    });


    // halaman yang di load default pertama kali
    $('.maps').load('{!! route('cianjur') !!}');            

  });
</script>
<script>
  $(function () {

    /* ChartJS */
    //-------------
    //- PIE CHART -
    //-------------

    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = {
      labels: [
      'Teknokratis',
      'Partisipatif', 
      'Politis',  
      ],
      datasets: [
      {
        data: [62,32,6],
        backgroundColor : ['#00BFFF', '#4682B4', '#20B2AA'],
      }
      ]
    };
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie 
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions      
    })

    /* END pie CHART */

        /*
     * BAR CHART
     * ---------
     */

     var bar_data = {
      data : [[1,10], [2,8], [3,4], [4,30]],
      bars: { show: true }
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
       bars: {
        show: true, barWidth: 0.5, align: 'center',
      },
    },
    colors: ['#3c8dbc'],
    xaxis : {
      ticks: [[1,'Triwulan 1'], [2,'Triwulan 2'], [3,'Triwulan 3'], [4,'Triwulan 4']]
    }
  })
    /* END BAR CHART */

  })

</script>
@stop
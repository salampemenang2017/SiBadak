	@extends('layouts.blank-page-dinas')
	@section('title')
	Dinas - Peta DAK Sekolah
	@stop
	@section('style-css')
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/dist/css/maps.css') }}">
	@stop
	@section('api-maps')
	<script src="{{ asset('/assets/dist/js/maps-dak-sekolah.js') }}"></script>
	{{-- API Google Maps --}}
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJgu7nyrc6KwvXMkgZwAy7eQYxdNPoYwo&libraries=places&callback=initMap" async defer></script>
	@stop
	@section('sidecontent')
	<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
		<li class="nav-item has-treeview">
			<a class="nav-link text-white">
				<i class="nav-icon fas fa-map"></i>
				<p class="text-white">
					Daftar Kecamatan 
					<i class="fas fa-angle-left right"></i>
				</p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Agrabinta</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Bojong Picung</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Campaka</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Campaka Mulya</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cianjur</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cibeber</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cibinong</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cidaun</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cijati</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cikadu</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cikalong Kulon</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cilaku</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cipanas</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Ciranjang</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Cugenang</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Gekbrong</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Haurwangi</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Kadupandak</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Karamg Tengah</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Leles</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Mande</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Naringgul</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Pacet</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Pagelaran</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Pasir Kuda</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Sindang Barang</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Sukaluyu</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Sukanagara</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Sukaresmi</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Takokak</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Tanggeung</p>
					</a>
				</li>
				<li class="nav-item has-treeview">
					<a class="nav-link">
						<i class="fas fa-angle-right nav-icon"></i>
						<p class="text-white">Warung Kondang</p>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	@stop
	@section('page-name')
	Peta Dana Alokasi Khusus Sekolah Kabupaten Cianjur
	@stop
	@section('content')
	<div class="content-wrapper py-4">
		<!-- Main content -->
		<section class="content">
			<div class="card">
				<div class="maps-posisi">
					<input type="text" class="form-control controls mt-2" placeholder="Cari Sekolah atau Kecamatan dikabupaten Cianjur..." id="pac-input">
					<div id="map"></div>
				</div>
			</div>
		</section>
	</div>
	@stop
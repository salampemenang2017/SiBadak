@extends('layouts.blank-page-dinas')
@section('title')
Dinas - Peta SMK Al-Ibrohimiyah
@stop
@section('style-css')
<style type="text/css">
	#map {
		height: 630px;
		width: 100%;  
	}
</style>
@stop
@section('api-maps')
<script type="text/javascript">
	function initMap() {
		var uluru = {
			lat: -6.811383, 
			lng: 107.232379,
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: uluru
		});

		var contentString = '<div class="row">\
		<div class="col-sm-4">\
		<img src="/img/logo/peringatan.png" width="100" height="100">\
		</div>\
		<div class="col-sm-8">\
		<h4>SMK Al-Ibrohimiyah</h4><br>\
		<strong class="text-danger">Verifikasi Ditolak!</strong>\
		</div>\
		</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: 'Uluru (Ayers Rock)'
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	}
</script>
{{-- API Google Maps --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJgu7nyrc6KwvXMkgZwAy7eQYxdNPoYwo&libraries=places&callback=initMap" async defer></script>
@stop
@section('page-name')
Peta SMKN 1 Karang Tengah
@stop
@section('content')
<div class="content-wrapper py-4">
	<!-- Main content -->
	<section class="content">
		<div class="card">
			<div class="maps-posisi">
				<div id="map">
					
				</div>
			</div>
		</div>
	</section>
</div>
@stop
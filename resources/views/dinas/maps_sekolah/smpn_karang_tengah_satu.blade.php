@extends('layouts.blank-page-dinas')
@section('title')
Dinas - Peta SMPN 1 Karang Tengah
@stop
@section('style-css')
<style type="text/css">
	#map {
		height: 630px;
		width: 100%;  
	}
</style>
@stop
@section('api-maps')
<script type="text/javascript">
	function initMap() {
		var uluru = {
			lat: -6.798307, 
			lng: 107.187875
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: uluru
		});

		var contentString = '<div class="row p-3">\
		<div class="col-sm-3">\
		<img src="/img/logo/smpn-1-karangtengah.png" width="80" height="80">\
		</div>\
		<div class="col-sm-9">\
		<h6>SMPN 1 Karang Tengah - Pencairan DAK</h6><br>\
		<strong class="text-dark">IDR.</strong> 235,000,000.- \
		<a href="/riwayat-daftar-kegiatan-dak/smp-karangtengah-1" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
		\
		</div>\
		</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: 'Uluru (Ayers Rock)'
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	}
</script>
{{-- API Google Maps --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJgu7nyrc6KwvXMkgZwAy7eQYxdNPoYwo&libraries=places&callback=initMap" async defer></script>
@stop
@section('page-name')
Peta SMPN 1 Karang Tengah
@stop
@section('content')
<div class="content-wrapper py-4">
	<!-- Main content -->
	<section class="content">
		<div class="card">
			<div class="maps-posisi">
				<div id="map">
					
				</div>
			</div>
		</div>
	</section>
</div>
@stop
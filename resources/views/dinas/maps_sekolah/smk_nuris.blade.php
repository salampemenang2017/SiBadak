@extends('layouts.blank-page-dinas')
@section('title')
Dinas - Peta SMK Nurul Islam Cianjur
@stop
@section('style-css')
<style type="text/css">
	#map {
		height: 630px;
		width: 100%;  
	}
</style>
@stop
@section('api-maps')
<script type="text/javascript">
	function initMap() {
		var uluru = {
			lat: -6.803912, 
			lng: 107.219360
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: uluru
		});

		var contentString = '<div class="row p-3">\
		<div class="col-sm-3">\
		<img src="/assets/dist/img/nuris.png" width="80" height="80">\
		</div>\
		<div class="col-sm-9">\
		<h6>SMK Nurul Islam Cianjur - Pencairan DAK</h6><br>\
		<strong class="text-dark">IDR.</strong> 35,000,000.- \
		<a href="/riwayat-daftar-kegiatan-dak" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
		\
		</div>\
		</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: 'Uluru (Ayers Rock)'
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	}
</script>
{{-- API Google Maps --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJgu7nyrc6KwvXMkgZwAy7eQYxdNPoYwo&libraries=places&callback=initMap" async defer></script>
@stop
@section('page-name')
Peta SMK Nurul Islam Cianjur
@stop
@section('content')
<div class="content-wrapper py-4">
	<!-- Main content -->
	<section class="content">
		<div class="card">
			<div class="maps-posisi">
				<div id="map">
					
				</div>
			</div>
		</div>
	</section>
</div>
@stop
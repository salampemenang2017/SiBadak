@extends('layouts.blank-page-dinas')
@section('title')
Daftar Verifikasi Pengajuan DAK
@stop
@section('page-name')
Daftar Verifikasi Pengajuan DAK 
@stop

@section('content')
<div class="content-wrapper py-4">
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <div class="info-box">
            <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-money-check-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Total Anggaran</strong></span>
              <span> ... </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>  
        <div class="col-lg-6">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-book"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Total Pengajuan</strong></span>
              <span>48</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="info-box">
            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check-double"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Pengajuan Diterima</strong></span>
              <span>40</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-box">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-stopwatch"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Pengajuan Menunggu</strong></span>
              <span>4</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-box">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Pengajuan Ditolak</strong></span>
              <span>4</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>      
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="far fa-chart-bar"></i>
                Daftar Pengajuan DAK
              </h3>
            </div>
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead class="bg-info">
                  <tr>
                    <th width="50"><center>Tanggal</center></th>
                    <th width="150"><center>Sekolah</center></th>
                    <th width="210"><center>Kegiatan</center></th>
                    <th width="50"><center>Sifat</center></th>
                    <th width="10"><center>File</center></th>
                    <th width="10"><center>Foto</center></th>
                    <th width="10"><center>Status</center></th>
                    <th width="130" class="text-center">Verifikasi ?</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <center>19/10/2019-09:12:55</center>
                    </td>
                    <td>
                      <center><b style="font-size: 14px;">SMK Nurul Islam Cianjur <a href="{{ route('maps-nuris') }}" title="Alamat SMK Nurul Islam"><i class="fas fa-map-marked-alt text-info"></i></b></center>
                    </td>
                    <td>
                      <center>
                        <span>Pengadaan kursi siswa dengan bahan kayu bayur 200 unit</span>
                        <br>
                        <strong class="text-success" style="font-size: 15px;"><i class="fa fa-money-bill-alt"></i> Rp. 35,000,000.-</strong>
                      </center>
                    </td>
                    <td class="text-warning">
                      <center>Partisipatif</center>
                    </td>
                    <td>
                      <center>
                        <a href="/pdf/contoh.pdf" download="">
                          <i class="fas fa-download text-info"></i>
                        </a>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#">
                          <i class="fas fa-images text-info" data-toggle="modal" data-target="#modal-xl"></i>
                        </a>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i id="stopwatch" class="fas fa-stopwatch text-info"></i>
                        <i id="check" class="fas fa-check-circle text-success" style="display: none;"></i>
                        <i id="checktolak" class="fas fa-times-circle text-danger" style="display: none;"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#" id="verifi?" data-toggle="modal" data-target="#modal-sm">
                          Verifikasi ?
                        </a>
                        <a href="#" id="terVerifi" class="text-success" style="display: none;">
                          ter Verifikasi
                        </a>
                        <a href="#" id="Verifitolak" class="text-danger" style="display: none;">
                          Verifikasi ditolak
                        </a>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <center>19/10/2019-09:12:55</center>
                    </td>
                    <td>
                      <center><b style="font-size: 15px;">SMK Al-Ibrohimiyah  <a href="{{ route('maps-bpc') }}" title="Alamat SMK Bunga Persada Cianjur"><i class="fas fa-map-marked-alt text-info"></i></a></b></center>
                    </td>
                    <td>
                      <center>
                        <span>Pembangunan ruang kelas baru</span>
                        <br>
                        <strong class="text-success" style="font-size: 15px;"><i class="fa fa-money-bill-alt"></i> Rp. 235,000,000.-</strong>
                      </center>
                    </td>
                    <td class="text-danger">
                      <center>Poilitis</center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-download text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-images text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center><i class="fas fa-times-circle text-danger"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#" class="text-danger"> 
                          verifikasi ditolak
                        </a>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <center>19/10/2019-09:12:55</center>
                    </td>
                    <td>
                      <center><b style="font-size: 15px;">SMPN 2 Sukaluyu <a href="{{ route('maps-smp_dua_sukaluyu') }}" title="Alamat SMPN 2 Sukaluyu"><i class="fas fa-map-marked-alt text-info"></i></a></b></center>
                    </td>
                    <td>
                      <center>
                        <span>Pembangunan Laboratorium (LAB) komputer 1 kelas</span>
                        <br>
                        <strong class="text-success" style="font-size: 15px;"><i class="fa fa-money-bill-alt"></i> Rp. 85,000,000.-</strong>
                      </center>
                    </td>
                    <td class="text-success">
                      <center>Teknokratis</center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-download text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-images text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-check-circle text-success"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#" class="text-success">Ter Verifikasi</a>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <center>19/10/2019-09:12:55</center>
                    </td>
                    <td>
                      <center><b style="font-size: 15px;">SMPN 1 Karang Tengah <a href="{{ route('maps-karatsu') }}" title="Alamat SMPN 1 Karan Tengah"><i class="fas fa-map-marked-alt text-info"></i></a></b></center>
                    </td>
                    <td>
                      <center>
                        <span>Pengadaan paket komputer pc 25 unit</span>
                        <br>
                        <strong class="text-success" style="font-size: 15px;"><i class="fa fa-money-bill-alt"></i> Rp. 235,000,000.-</strong>
                      </center>
                    </td>
                    <td class="text-danger">
                      <center>politis</center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-download text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-images text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-check-circle text-success"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#" class="text-success">Ter Verifikasi</a>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <center>19/10/2019-09:12:55</center>
                    </td>
                    <td>
                      <center><b style="font-size: 15px;">SMPN 1 Ciranjang <a href="{{ route('maps-andir') }}" title="Alamat SMPN 1 Karan Tengah"><i class="fas fa-map-marked-alt text-info"></i></a></b></center>
                    </td>
                    <td>
                      <center>
                        <span>Pengadaan paket komputer pc 25 unit</span>
                        <br>
                        <strong class="text-success" style="font-size: 15px;"><i class="fa fa-money-bill-alt"></i> Rp. 235,000,000.-</strong>
                      </center>
                    </td>
                    <td class="text-danger">
                      <center>politis</center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-download text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-images text-info"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <i class="fas fa-check-circle text-success"></i>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a href="#" class="text-success">Ter Verifikasi</a>
                      </center>
                    </td>
                  </tr>
                </tbody>
                {{-- <tfoot class="bg-info">
                  <tr>
                    <th width="50"><center>Tanggal</center></th>
                    <th width="150"><center>Sekolah</center></th>
                    <th width="210"><center>Kegiatan</center></th>
                    <th width="50"><center>Sifat</center></th>
                    <th width="10"><center>File</center></th>
                    <th width="10"><center>Foto</center></th>
                    <th width="10"><center>Status</center></th>
                    <th width="130" class="text-center">Verifikasi ?</th>
                  </tr>
                </tfoot> --}}
              </table>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <p>Yakin pengajuan DAK di verifikasi ?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" onclick="changeStatustolak()" data-dismiss="modal" class="btn btn-danger swalDefaultError">Tolak</button>
        <button type="button" onclick="changeStatus()" data-dismiss="modal" class="btn btn-primary swalDefaultSuccess">Verifikasi</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Foto SMP NEGERI 2 CIKADU</h4>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Keluar</button>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="colsm">
                  <i class="fas fa-info-circle"></i>  
                </div>
                <div class="col">
                  <h6>Pengadaan kursi siswa dengan bahan kayu bayur 200 unit</h6>
                </div>
              </div> 
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-md-4">
              <div class="card" style="width: 18rem;" data-toggle="modal" data-target="#modal-lg">
                <a href="#">
                  <img style="height: 230px;" src="https://media.karousell.com/media/photos/products/2019/01/13/dijual_tanah_2_hektardi_vill_cinere_mas_raya_jakarta_selatan_1547383161_9c214c59.jpg" class="card-img-top" id="foto1">
                </a>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-9">
                      <small class="text-muted">Diunggah <strong>27 September 2017</strong></small> 
                    </div>
                    <div class="col-md-1">
                      <span class="badge badge-danger">Sebelum</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                <a href="#">
                  <img style="height: 230px;" src="http://2.bp.blogspot.com/_EfS7B8QfMOI/TQTAxCJfkrI/AAAAAAAAANg/2aW-Ah2Ix24/s1600/100710155346.jpg" class="card-img-top" id="foto2">
                </a>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-9">
                      <small class="text-muted">Diunggah <strong>27 Oktober 2017</strong></small> 
                    </div>
                    <div class="col-md-1">
                      <span class="badge badge-warning">Proses</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                <a href="#">
                  <img style="height: 230px;" src="https://cdn2.tstatic.net/palembang/foto/bank/images/bangunan-rkb-smpn-1-talangpadang232.jpg" class="card-img-top" id="foto3">
                </a>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-9">
                      <small class="text-muted">Diunggah <strong>23 Desember 2017</strong></small> 
                    </div>
                    <div class="col-md-1">
                      <span class="badge badge-primary">Selesai</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-lg">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Pengadaan kursi siswa dengan bahan kayu bayur 200 unit
          <span class="badge badge-danger">Sebelum</span>
        </h4>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Keluar</button>
      </div>
      <div class="modal-body">
        <div id="demo" class="carousel slide" data-ride="carousel">
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="https://media.karousell.com/media/photos/products/2019/01/13/dijual_tanah_2_hektardi_vill_cinere_mas_raya_jakarta_selatan_1547383161_9c214c59.jpg" width="1100" height="500"> 
            </div>
            <div class="carousel-item">
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTA034xAlbjms-4PEIu0KOVGzfxMCITWkLK2hfAgpnDj_qhkSO1" alt="Chicago" width="1100" height="500">
            </div>
            <div class="carousel-item">
              <img src="http://www.medanbisnisdaily.com/imagesfile/201406/20140616084613_549.gif" alt="New York" width="1100" height="500"> 
            </div>
          </div>
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@stop


@section('script-js')
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

  function changeStatus() {
    document.getElementById('stopwatch').style.display = "none";
    document.getElementById('check').style.display = "block";
    document.getElementById('verifi?').style.display = "none";
    document.getElementById('terVerifi').style.display = "block";
  }

  function changeStatustolak() {
    document.getElementById('stopwatch').style.display = "none";
    document.getElementById('checktolak').style.display = "block";
    document.getElementById('verifi?').style.display = "none";
    document.getElementById('Verifitolak').style.display = "block";
  }

  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    
    $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        type: 'success',
        title: 'Pengajuan DAK telah di verifikasi.'
      })
    });

    $('.swalDefaultError').click(function() {
      Toast.fire({
        type: 'error',
        title: 'Pengajuan DAK telah di tolak.'
      })
    });

  });
</script>
@stop
@extends('layouts.operator.master')

@section('rute', '/operator-dashboard')

@section('menu', 'Pengajuan DAK')

@section('title', 'Form-Input DAK')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 col-sm-12 sol-xs-12">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#kegiatan" aria-controls="kegiatan" role="tab" data-toggle="tab" style="margin-right: 10px;">Kegiatan</a></li>
				<li role="presentation"><a href="#upload" aria-controls="upload" role="tab" data-toggle="tab" style="margin-right: 10px;">Upload Foto</a></li>
				<li role="presentation"><a href="#tampil" aria-controls="tampil" role="tab" data-toggle="tab">Tampil Data</a></li>
			</ul>
			<!-- End Nav Tabs -->
		</div>

		<!-- Tab panes -->
		<div class="tab-content">
			<!-- Start Kegiatan -->
			<div role="tabpanel" class="tab-pane active" id="kegiatan">
				<div class="col-sm-12" style="margin-top: 15px;">
					<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i></span> Pengajuan DAK (Form Kegiatan)</h4>
					<div class="container">
						<p style="margin-bottom: -10px; margin-top: 10px;" align="justify">Silahkan Anda Isi Data Pengajuan DAK Online ini Jenis, Anggaran, Kegiatan dan File Proposal</p>
					</div>

					<hr style="border: 1px solid black;">
				</div>

				<form action="#">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="form-group">
							<label for="jp">Jenis Pekerjaan</label>
							<select name="jp" class="form-control" id="jp">
								<option selected="" disabled="">- Silahkan Pilih -</option>
								<option>Membangun Kelas</option>
								<option>Membangun Toilet</option>
								<option>Membeli Peralatan</option>
							</select>
						</div>
					</div>

					<div class="col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-sm-6 col-xs-6">
								<div class="form-group">
									<label for="jumlah">Jumlah</label>
									<input type="number" name="jumlah" id="jumlah" class="form-control" id="jumlah" onclick="onType()">
								</div>
							</div>

							<div class="col-sm-6 col-xs-6">
								<div class="form-group">
									<label for="satuan">Satuan</label>
									<input type="text" name="satuan" id="satuan" class="form-control" id="satuan" onclick="onType()">
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-10 col-sm-offset-1">
						<div class="form-group">
							<label for="anggaran">Nilai Anggaran</label>
							<input type="number" name="anggaran" class="form-control" id="anggaran" onclick="onType()">
						</div>
					</div>

					<div class="col-sm-10 col-sm-offset-1">
						<div class="form-group">
							<label for="deskripsi">Deskripsi Kegiatan</label>
							<textarea class="form-control" name="deskripsi" id="deskripsi" onclick="onType()"></textarea>
						</div>
					</div>

					<div class="col-sm-10 col-sm-offset-1">
						<div class="form-group">
							<label for="jp">Sifat Pekerjaan</label>
							<select name="jp" class="form-control" id="jp">
								<option selected="" disabled="">- Silahkan Pilih -</option>
								<option>Tenokratis</option>
								<option>Partisipatif</option>
								<option>Politis</option>
							</select>
						</div>
					</div>

					<div class="col-sm-10 col-sm-offset-1">
						<div class="form-group">
							<label for="proposal">Upload Proposal</label>
							<div class="row">
								<div class="col-md-10 col-sm-8 col-xs-8">
									<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile">
									<input type="file" name="proposal" id="proposal" style="display: none;" accept=".png, .jpg, .jpeg, .pdf">
								</div>

								<div class="col-md-2 col-sm-4 col-xs-4">
									<label class="btn btn-primary" for="proposal">Browse</label>
								</div>
							</div>
						</div>
					</div>
					
					<br />

					<div class="col-sm-4 col-sm-offset-4 text-center">
						<button class="btn btn-md btn-info swalDefaultSuccess" style="width: 200px;">Selanjutnya</button>
					</div>
				</form>
			</div>
			<!-- End Kegiatan -->

			<!-- Start Upload -->
			<div role="tabpanel" class="tab-pane" id="upload">
				<div class="col-sm-12" style="margin-top: 15px;">
					<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i> </span>Pengajuan DAK (Upload Foto)</h4>
					<div class="container">
						<p style="margin-bottom: -10px; margin-top: 10px;" align="justify">Upload Data Foto Existing Terlebih Dahulu Selanjutnya jika Ada Progres dan Selesai Laporkan Kembali</p>
					</div>

					<hr style="border: 1px solid black;">
				</div>

				<div class="col-sm-10 col-sm-offset-1 text-center">
					<div class="image">
						<a href="{{ route('uploadB') }}"><i class="fa fa-camera camera"></i></a>
						<p>Foto Kegiatan Sebelum</p>
					</div>

					<div class="image">
						<a href="{{ route('uploadP') }}"><i class="fa fa-camera camera"></i></a>
						<p>Foto Kegiatan Pengerjaan</p>
					</div>

					<div class="image">
						<a href="{{ route('uploadE') }}"><i class="fa fa-camera camera"></i></a>
						<p>Foto Kegiatan Selesai</p>
					</div>
				</div>
			</div>
			<!-- End Uplaod -->

			<!-- Start Tampli-Data -->
			<div role="tabpanel" class="tab-pane" id="tampil">
				<div class="col-sm-12" style="margin-top: 15px;">
					<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i> </span>Pengajuan DAK (Riwayat DAK)</h4>
					<div class="container">
						<p style="margin-bottom: -10px; margin-top: 10px;" align="justify">Anda Sedang Melihat Daftar Pengajuan Kegiatan Update RAD Secara Realtime</p>
					</div>
					<hr style="border: 1px solid black;">
				</div>

				<div class="col-sm-12" style="margin-bottom: 10px;">
					<p>Daftar Pengajuan Kegiatan</p>
				</div>

				<div class="col-sm-10 col-sm-offset-1">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-md-8 col-sm-8 col-xs-6">
								<p class="text-primary" align=justify>Pembangunan Toilet Umum 4 Pintu Dan Tempat Wudhu</p> 
								<br />
								<span class="text-success"><i class="fa fa-cc"></i></span>
								<span style="color: black;">Rp. 20,1111,000.-</span>
								<span><i class="fa fa-pencil"></i></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="accept">approve</span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span>
									<a href="{{ route('pengeluaran1') }}" class="text-warning"><i class="fa fa-money"></i></a>
									&nbsp;
									<a href="#" class="text-warning"><i class="fa fa-camera"></i></a>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-md-8 col-sm-8 col-xs-6">
								<p class="text-primary" >Pengadaan Komputer Siswa 50 Unit</p> 
								<br />
								<span class="text-success"><i class="fa fa-cc"></i></span>
								<span style="color: black;">Rp. 120,500,000.-</span>
								<span><i class="fa fa-pencil"></i></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="reject">reject</span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="text-warning disable">
									<i class="fa fa-money"></i>
									&nbsp;
									<i class="fa fa-camera"></i>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1">
					<div class="alert alert-info">
						<div class="row">

							<div class="col-md-8 col-sm-8 col-xs-6">
								<p class="text-primary">Pembangunan Ruang Kelas Baru Dengan Ukuran 40 X 40 Pondasi Anti Gempa</p> 
								<br />
								<span class="text-success"><i class="fa fa-cc"></i></span>
								<span style="color: black;">Rp. 128,233,000.-</span>
								<span><i class="fa fa-pencil"></i></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="accept">approve</span>							
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span>
									<a href="{{ route('pengeluaran2') }}" class="text-warning"><i class="fa fa-money"></i></a>
									&nbsp;
									<a href="#" class="text-warning"><i class="fa fa-camera"></i></a>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1">
					<div class="alert alert-info">
						<div class="row">
							<div class="col-md-8 col-sm-8 col-xs-6">
								<p class="text-primary" >Pengadaan Kursi Siswa 50 Unit</p> 
								<br />
								<span class="text-success"><i class="fa fa-cc"></i></span>
								<span style="color: black;">Rp. 12,233,000.-</span>
								<span><i class="fa fa-pencil"></i></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="waiting bg-primary">waiting</span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3">
								<span class="text-warning disable">
									<i class="fa fa-money"></i>
									&nbsp;
									<i class="fa fa-camera"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Tampil-Data -->
		</div>
		<!-- End Tab-panes -->
	</div>
</div>

@section('id')
id="nav"
@stop

<!-- Style CSS -->
<style>
	.reject{
		color: #f1f2f6;
		background-color: #eb3b5a;
		padding: 3px;
		border-radius: 3px;
		margin-left: : -10px;
	}

	.accept{
		color: #f1f2f6;
		background-color: #2ed573;
		padding: 3px;
		border-radius: 3px;
	}

	.waiting{
		color: #f1f2f6;
		padding: 3px;
		border-radius: 3px;
	}

	.disable {
		color: gray;
	}

	.image {
		border: 1px solid #45aaf2;
		border-style: dashed;
		padding: 10px;
		margin-top: 30px;

	}

	#random_foto1 {
		width: 50%;
		margin: auto;
	}

	.image p {
		color: #45aaf2;
	}

	.camera {
		color: #45aaf2;
		font-size: 100px;
	}
</style>

<script>
// Function UploadFile (Value)
document.getElementById('proposal').onchange = function () {
	document.getElementById('uploadFile').value = this.value;
}

// Function On Typing
function onType() {
	document.getElementById('nav').style.position = "relative";
}

// Swall-Alert
$(function() {
	const Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});

	$('.swalDefaultSuccess').click(function() {
		Toast.fire({
			type: 'success',
			title: 'Data Berhasil Disimpan'
		})
	});

});
</script>


@stop


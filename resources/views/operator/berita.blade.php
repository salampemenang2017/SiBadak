@extends('layouts.operator.master')

@section('rute', '/operator-dashboard')

@section('menu', 'Info Berita')

@section('title', 'Form-Input MasterSekolah')

@section('content')
<div class="container">
    <div class="col-sm-12" style="margin-top: 18px;">
    <h3>Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</h3>
    <img src="{{asset('img/berita/berita-1.jpg')}}" width="100%" alt="Gambar" class="rounded img-fluid" style="margin-top: 15px; margin-bottom: 15px;">
    <p style="text-align: justify;">KBRN, Cianjur : Guna meningkatkan roda pembangunan di Cianjur khususnya di Desa Sukawangi Kecamatan Warungkondang Kabupaten Cianjur,  Plt. Bupati Cianjur H. Herman Suherman melaksanakan kunjungan ke desa Sukawangi dalam rangka Cianjur Ngawangun Lembur (CNL), Kamis (8/8/2019).
    Salah satu tujuan kegiatan itu, menurut Plt. Bupati Cianjur H. Herman Suherman bahwa beberapa pelayanan pada masyarakat telah dilaksanakan oleh masing-masing OPD diantaranya pelayanan pembuatan KTP, Pembuatan izin usaha, Kesehatan,  dan Pertanian. 
    "Desa Sukawangi merupakan wilayah dasar pembangunan bagi kemajuan kabupaten Cianjur. "Terangnya. 
    Dalam acara Cianjur Ngawangun Lembur, H. Herman Suherman sekaligus memberikan santunan kepada anak yatim piatu, para jompo dan melantik ikatan remaja masjid di setiap desa yang dikunjungi. 
    "Untuk menumbuhkembangkan agama Islam di masing-masing desa khusus para remaja baik remaja putera maupun putri. "Tutupnya</p>   
    </div>
</div>

@stop
@extends('layouts.operator.master')

@section('rute', '/dak-master')

@section('menu', 'Upload')

@section('title', 'Upload-Foto Sebelum')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12" style="margin-bottom: 50px;">
			<div class="container" style="margin-bottom: -10px;">
				<h4 style="font-weight: bold; color: #3498db; font-size: 19px;"><i class="fa fa-info-circle"></i> Form DAK (Upload Foto Sebelum)</h4>
			</div>
		</div>

		<div class="col-sm-10 col-sm-offset-1">
			<form action="#">
				<!-- Foto 1 -->
				<div class="form-group">
					<label>Foto 1</label>
					<div class="row">
						<div class="sol-sm-12 col-xs-12 text-center">
							<div class="image" id="img1" style="display: none;">
								<img src="#" id="random_foto1" width="50%" style="display: none;">
							</div>
						</div>

						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." id="uploadFile1">
							<input type="file" name="fkb1" id="fkb1" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="fkb1">Browse</label>
						</div>
					</div>
				</div>

				<hr style="border-bottom: 1px solid black">

				<!-- Foto 2 -->
				<div class="form-group">
					<label>Foto 2</label>
					<div class="row">
						<div class="sol-sm-12 col-xs-12 text-center">
							<div class="image" id="img2" style="display: none;">
								<img src="#" id="random_foto2" width="50%" style="display: none;">
							</div>
						</div>

						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." id="uploadFile2">
							<input type="file" name="fkb2" id="fkb2" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="fkb2">Browse</label>
						</div>
					</div>
				</div>

				<hr style="border-bottom: 1px solid black">

				<!-- Foto 3 -->
				<div class="form-group">
					<label>Foto 3</label>
					<div class="row">
						<div class="sol-sm-12 col-xs-12 text-center">
							<div class="image" id="img3" style="display: none;">
								<img src="#" id="random_foto3" width="50%" style="display: none;">
							</div>
						</div>

						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." id="uploadFile3">
							<input type="file" name="fkb3" id="fkb3" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="fkb3">Browse</label>
						</div>
					</div>
				</div>

				<br />

				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-4 text-center">
						<button class="btn btn-sm btn-info swalDefaultSuccess" style="width: 200px;">Simpan</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<style>
	.image {
		padding: 10px;
		margin-bottom: 10px;
	}

</style>

<script>
// Upload-Foto 1
document.getElementById('fkb1').onchange = function () {
	document.getElementById('uploadFile1').value = this.value;
}

$("#fkb1").change(function(event) {  
	fadeInAdd();
	getURL(this);    
});

$("#fkb1").on('click',function(event){
	fadeInAdd();
});

function getURL(input) {    
	if (input.files && input.files[0]) {   
		var reader = new FileReader();
		var filename = $("#fkb1").val();
		filename = filename.substring(filename.lastIndexOf('\\')+1);
		reader.onload = function(e) {
			debugger;      
			$('#random_foto1').attr('src', e.target.result);
			$('#random_foto1').hide();
			$('#random_foto1').fadeIn(500);      
			$('.custom-file-label').text(filename);             
		}
		reader.readAsDataURL(input.files[0]);    
	}
	$(".alert").removeClass("loadAnimate");
}

function fadeInAdd(){
	fadeInAlert();  
}
function fadeInAlert(text){
	$(".alert").text(text).addClass("loadAnimate"); 
	document.getElementById('img1').style.display = "block";
}

// Upload-Foto 2
document.getElementById('fkb2').onchange = function () {
	document.getElementById('uploadFile2').value = this.value;
}

$("#fkb2").change(function(event) {  
	fadeInAdd2();
	getURL2(this);    
});

$("#fkb2").on('click',function(event){
	fadeInAdd2();
});

function getURL2(input) {    
	if (input.files && input.files[0]) {   
		var reader = new FileReader();
		var filename = $("#fkb2").val();
		filename = filename.substring(filename.lastIndexOf('\\')+1);
		reader.onload = function(e) {
			debugger;      
			$('#random_foto2').attr('src', e.target.result);
			$('#random_foto2').hide();
			$('#random_foto2').fadeIn(500);      
			$('.custom-file-label').text(filename);             
		}
		reader.readAsDataURL(input.files[0]);    
	}
	$(".alert").removeClass("loadAnimate");
}

function fadeInAdd2(){
	fadeInAlert2();  
}
function fadeInAlert2(text){
	$(".alert").text(text).addClass("loadAnimate"); 
	document.getElementById('img2').style.display = "block";
}


// Upload-Foto 3
document.getElementById('fkb3').onchange = function () {
	document.getElementById('uploadFile3').value = this.value;
}

$("#fkb3").change(function(event) {  
	fadeInAdd3();
	getURL3(this);    
});

$("#fkb3").on('click',function(event){
	fadeInAdd3();
});

function getURL3(input) {    
	if (input.files && input.files[0]) {   
		var reader = new FileReader();
		var filename = $("#fkb3").val();
		filename = filename.substring(filename.lastIndexOf('\\')+1);
		reader.onload = function(e) {
			debugger;      
			$('#random_foto3').attr('src', e.target.result);
			$('#random_foto3').hide();
			$('#random_foto3').fadeIn(500);      
			$('.custom-file-label').text(filename);             
		}
		reader.readAsDataURL(input.files[0]);    
	}
	$(".alert").removeClass("loadAnimate");
}

function fadeInAdd3(){
	fadeInAlert3();  
}
function fadeInAlert3(text){
	$(".alert").text(text).addClass("loadAnimate"); 
	document.getElementById('img3').style.display = "block";
}


// Swall-Alert
	$(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000
    });
    
    $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        type: 'success',
        title: 'Data Berhasil Disimpan'
      })
    });

  });


</script>

@stop


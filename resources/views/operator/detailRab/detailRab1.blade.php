@extends('layouts.operator.master')

@section('rute', '/pengeluaran1')

@section('menu', 'Detail RAB 1')

@section('title', 'Form-Input Pengeluaran')

@section('content')
<div class="container">
	<div class="row">
        <div class="col-sm-12">
			<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i></span> Detail RAB</h4>
			<div class="container">
				<p style="margin-bottom: -10px; margin-top: 10px;">Menampilkan Data Rekapan Per Kegiatan atau Pekerjaan</p>
			</div>

			<hr style="border: 1px solid black;">
		</div>

        <!-- Alert -->
		<div class="col-sm-10 col-sm-offset-1">
            <div class="alert alert-info" style="height: auto;">
                <div class="row">
                    <div class="col-sm-2 col-xs-2">
                        <span class="text-danger"><i class="fa fa-warning fa-3x"></i></span> 
                    </div>
                    <div class="col-sm-10 col-xs-10">
                        Pembangunan Toilet Sekolah 4 Pintu Dan Tempat Wudhu
                    </div>
                </div>
			</div>
        </div>
        <!-- End Alert -->

        <!-- Log List -->
        <div class="container" style="margin-top: 10px;">
            <h4><b>Daftar</b></h4>
        </div>
        <br>

            <div class="col-md-12">
                <!-- <div class="row"> -->
                    <table class="table table-default" id="mytable">
                        <thead>
                            <tr style="background:#45aaf2; border-color: #0abde3; color: #fff;">
                                <th width="10">Desk.</th>
                                <th>Vol / Set</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Pengukuran Pekerjaan</td>
                                <td>1 / Is</td>
                                <td>150,000</td>
                                <td>150,000</td>
                                <td><i class="fa fa-trash swalDefaultError"></i></td>
                            </tr>
                            <tr>
                                <td>Rek. Pasang Pondasi Batu</td>
                                <td>30 / m</td>
                                <td>5,000</td>
                                <td>150,000</td>
                                <td><i class="fa fa-trash"></i></td>
                            </tr>
                            <tr>
                                <td>Rek. Pasang Batu Bata</td>
                                <td>35 / m2</td>
                                <td>150,000</td>
                                <td>150,000</td>
                                <td><i class="fa fa-trash"></i></td>
                            </tr>
                            <tr>
                                <td>Pembelian Semen</td>
                                <td>10 / zak</td>
                                <td>50,000</td>
                                <td>600,000</td>
                                <td><i class="fa fa-trash"></i></td>
                            </tr>
                        </tbody>
                    </table><br>
                    <table class="table table-default">
                        <tr style="background:#45aaf2; border-color: #0abde3; color: #fff;">
                            <td align="center"><h4 class="text-white"><b>TOTAL : 1,425,000.-</b</h4></td>
                        </tr>
                    </table>
                <!-- </div> -->
            </div>
        
        <!-- End Log List -->
		
	</div>
</div>
@stop

@push('scripts')
<script>

   var index, table = document.getElementById('mytable');
   for(var i = 0; i < table.rows.length; i++)
   {
    table.rows[i].cells[4].onclick = function()
    {
        var c = confirm("Apakah Yakin Mau Hapus Data?");
        if(c === true){
            index = this.parentElement.rowIndex;
            table.deleteRow(index);
            console.log(index);
        }
    };
}

</script>
@endpush









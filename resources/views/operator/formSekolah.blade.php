@extends('layouts.operator.master')

@section('rute', '/operator-dashboard')

@section('menu', 'Data Sekolah')

@section('title', 'Form-Input MasterSekolah')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i></span> Data Master Sekolah</h4>
			<div class="container">
				<p style="margin-bottom: -10px; margin-top: 10px;">Silahkan Anda Isi Data Master Sekolah Ini Sebelum Melakukan Pengajuan DAK Online</p>
			</div>

			<hr style="border: 1px solid black;">
		</div>

		<form action="#">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="klp">Kategori Lokasi Prioritas</label>
					<select name="klp" class="form-control" id="klp">
						<option selected="" disabled="">- Silahkan Pilih -</option>
						<option>Pilihan 1</option>
						<option>Pilihan 2</option>
						<option>Pilihan 3</option>
					</select>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="ns">Nama Sekolah</label>
					<input type="text" name="namaSekolah" id="ns" class="form-control" onclick="onType()">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label>Logo Sekolah</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile1">
							<input type="file" name="ls" id="ls" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="ls">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label>Scan Ijin Oprasinal Sekolah</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile2">
							<input type="file" name="sios" id="sios" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="sios">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label>Scan SK Kepsek</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile3">
							<input type="file" name="sios" id="ssk" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="ssk">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label>Scan SK Komite Sekolah</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile4">
							<input type="file" name="ssks" id="ssks" style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="ssks">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label>Scan Bk.Tabungan Sekolah</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile5">
							<input type="file" name="sbts" id="sbts"  style="display: none;" accept=".png, .jpg, .jpeg">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="sbts">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="row">
					<div class="col-sm-6 col-xs-6">
						<div class="form-group">
							<label for="jsti">Jml.Siswa.Thn.ini</label>
							<input type="number" name="jsti" id="jsti" class="form-control" onclick="onType()">
						</div>
					</div>

					<div class="col-sm-6 col-xs-6">
						<div class="form-group">
							<label for="jsts">Jml.Siswa.Thn.Sblm</label>
							<input type="number" name="jsts" id="jsts" class="form-control" onclick="onType()">
						</div>
					</div>
				</div>
			</div>

			<br />

			<div class="col-sm-4 col-sm-offset-4 text-center">
				<button type="submit" class="btn btn-md btn-info swalDefaultSuccess" style="width: 200px;">Simpan</button>
			</div>
		</form>
	</div>
</div>

@section('id')
	id="nav"
@stop

@stop

@push('scripts')
<script>

	// Function Upload (Value)

		// value - 1
		document.getElementById('ls').onchange = function () {
			document.getElementById('uploadFile1').value = this.value;
		}

		// value - 2
		document.getElementById('sios').onchange = function () {
			document.getElementById('uploadFile2').value = this.value;
		}

		// value - 3
		document.getElementById('ssk').onchange = function () {
			document.getElementById('uploadFile3').value = this.value;
		}

		// value - 4
		document.getElementById('ssks').onchange = function () {
			document.getElementById('uploadFile4').value = this.value;
		}

		// value - 5
		document.getElementById('sbts').onchange = function () {
			document.getElementById('uploadFile5').value = this.value;
		}


	// Function On Typing
	function onType() {
		document.getElementById('nav').style.position = "relative";
	}

	// Swall-Alert
	$(function() {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		$('.swalDefaultSuccess').click(function() {
			Toast.fire({
				type: 'success',
				title: 'Data Berhasil Disimpan'
			})
		});

	});
</script>
@endpush
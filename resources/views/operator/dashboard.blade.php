@extends('layouts.operator.blank')

@section('title')
    Dashboard User
@stop

@section('content')

    <!-- Atas  -->
    <div class="kotak gradient1">
        <div class="container">
            <!-- Jam & Title  -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-sm-6 col-xs-6 pull-left" style="color: #fff; font-size: 15px; ">
                    <span id="jam"></span>
                    <span> :</span>
                    <span id="menit"></span>
                    <span> :</span>
                    <span id="detik"></span>
                </div>

                <div class="col-sm-6 col-xs-6">
                    <p class="pull-right" style="color: #fff; font-size: 17px; font-weight: bold;">Dashboard</p>
                </div>

                <div class="col-sm-12 col-xs-12">
                    <hr style="border-color: #ddd; width: 100%; margin-top: 4px;">
                </div>
            </div>
            <!-- End Jam & Title  -->

            <!-- Welcome Operator  -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-sm-8 col-xs-8" style="color: #fff;">
                    <h4 style="font-weight: bold;">SMK Nurul Islam</h4>
                    <hr style="border-color: #ddd; width: 100%; margin-top: 4px; margin-bottom: 2px;">
                    <p style="display: inline-block;" id="sapa"></p> 
                    <span style="display: inline-block;">Operator Sekolah</span>
                </div>

                <div class="col-sm-4 col-xs-4 text-center">
                    <img src="{{ asset('/assets/fixHealt/img/smaknis.png') }}" alt="" width="80%" style="margin-top: -10px;">
                </div>
            </div>
            <!-- End Welcome Operator  -->

            <!-- Quick Menu -->
              <div class="col-sm-12" style="margin-top: 50px;">
                <div style="background-color: #fff; padding-top: 20px; padding-bottom: 20px; border-radius: 5px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);       
                    transition: 0.3s; border: 1px #fff;">
                    <div class="col-sm-12">
                        <p style="font-size: 20px; font-weight: bold;">SI-<span style="color: #45AAF2;">BADAK</span></p>
                        <span style="font-size: 12px; color: gray;">Memudahkan Anda Dalam Melakukan Pengajuan DAK</span>
                        <hr style="border-color: #111; width: 100%; margin-top: 4px;">
                    </div>

                    <div class="container">
                        <div class="col-sm-12 col-md-offset-2">
                  
                            <div class="row text-center">
                                <div class="col-sm-3 col-xs-4">
                                    <a href="{{ route('operator-halberita') }}">
                                        <i class="fa fa-newspaper-o fa-3x"></i><h6>Berita</h6>
                                    </a>
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <a href="{{ route('form-masterSekolah') }}">
                                        <i class="fa fa-graduation-cap fa-3x"></i><h6>Sekolah</h6>
                                    </a>
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <a href="{{ route('dak-master') }}">
                                        <i class="fa fa-cloud-upload fa-3x"></i><h6>Pengajuan</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-12 col-md-offset-2" style="margin-top: 20px;"> 
                            <div class="row text-center">
                                <div class="col-sm-3 col-xs-4">
                                    <a href="{{ route('form-pengeluaran') }}">
                                        <i class="fa fa-credit-card fa-3x"></i><h6>Pengeluaran</h6>
                                    </a> 
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <a href="{{ route('operator-profile') }}">
                                        <i class="fa fa-user fa-3x"></i><h6>Profile</h6>
                                    </a>
                                </div>
                                <div class="col-sm-3 col-xs-4">
                                    <a href="/">
                                        <i class="fa fa-power-off fa-3x"></i><h6>Logout</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Quick Menu -->
        </div>
    </div>

    <!-- Bawah -->
    <div class="container" style="margin-top: 190px; position: absolute; margin-bottom: 25px;">
       <!-- Berita -->
        <div>
           <p style="margin-top: 30px; font-weight: bold; font-size: 15px;">Berita Terbaru</p>
           <hr style="border-color: #aaa; width: 100%; margin-top: 6px;">
           <div class="col-sm-12" style="margin-top: 20px; margin-bottom: 10px;" id="berita">
            <img src="{{asset('img/berita/berita-1.jpg')}}" width="100%" alt="Gambar" class="rounded img-fluid" style="border-radius: 3px;">
            <p style="margin-top: 5px;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
            <a href="{{route('operator-berita')}}" class="btn btn-primary btn-sm pull-right">LIHAT BERITA</a href="{{route('operator-berita')}}">
            </div>  
        </div>

        <div style="margin-top: 50px;">
           <div class="col-sm-12" style="margin-top: 20px; margin-bottom: 10px;" id="berita">
            <img src="{{asset('img/berita/berita-1.jpg')}}" width="100%" alt="Gambar" class="rounded img-fluid" style="border-radius: 3px;">
            <p style="margin-top: 5px;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
            <a href="{{route('operator-berita')}}" class="btn btn-primary btn-sm pull-right">LIHAT BERITA</a href="{{route('operator-berita')}}">
            </div>
            <br /><br /><br /><br />  
        </div>
       <!-- End Berita -->      
    </div>
    
@stop

@push('scripts')
<script src="{{ asset('/assets/fixHealt/js/jam.js') }}"></script>
<script src="{{ asset('/assets/fixHealt/js/sapa.js') }}"></script>

<script>
$(document).ready(function() {
$(".down").click(function() {
     $('html, body').animate({
         scrollTop: $("#berita").offset().top
     }, 1500);
 });
});
</script>
@endpush

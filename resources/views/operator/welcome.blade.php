<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{asset('assets/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- CSS -->
    <style> 
        /* Preloader */
        #splash{
            width:100%;
            height:100%;  
            position:absolute;
            z-index:1;
            background:#fff;
        }

        .btnD{
            background-color: #45aaf2; 
            border: none;
            color: white;
            padding: 3px 24px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 2px;
            cursor: pointer;
            font-size: 14px;
        }

        .footer{
            position: fixed;
	        bottom: 0;
            left: 20%;
            right: 20%;
        }

    </style>
</head>
<body class="bg-white login-page">

    <div id="splash">

    <div class="container-fluid mt-3">
        <div class="col-md-11">
            <div class="row">
                <!-- <h4><i class="fa fa-arrow-left"></i></h4> &nbsp; <p class="mt-1">back to splash screen</p> -->
            </div>
        </div>
    </div>


    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-md-12 mt-5">
            </div>
            <div class="col-md-12 mt-5">
                <div class="text-center"><img src="{{asset('img/logo/infoSb.png')}}" width="200" height="150" alt="" style="margin-bottom: -75px;"></div>
                <h1 class="text-center mt-5" style="font-weight: bold; font-size: 60px; margin-bottom: -25px;">Si<span style="color: #45aaf2;">Badak</span></h1><br> <!--style="color: #ff7200;"-->
                <p style="text-align: justify; font-size: 14px; margin-bottom: 4px;" class="text-center"><i>(Sistem Informasi Bantuan Anggaran Dana Alokasi Khusus)</i></p>
                <div class="text-center">
                    <button class="btnD">Dinas Pendidikan Kabupaten Cianjur</button>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer">
        <p class="text-center" style="margin-bottom: 2px;">Development By</p>
        <h6 class="text-center">PT. ASQI DIGITAL INNOVATION</h6>
    </footer>

    </div>

    <div class="login-box" id="login">
        <div class="login-logo">
            <a href="../../index2.html"><b style="font-weight: bold;">Si</b><b style="color: #45aaf2; font-weight: bold;">BADAK</b></a>
        </div>
        <!-- /.login-logo -->
        <div>
            <div class="container">
            <!-- <p class="login-box-msg">Sign in to start your session</p> -->

            <!-- <form> -->
                <div class="input-group mb-3">
                <input type="email" class="form-control" placeholder="Email / Username">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                    </div>
                </div>
                </div>
                <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Password">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                    </div>
                </div>
                </div>

                <div class="col-7">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember">
                        <label for="remember">
                            Remember Me
                        </label>        
                    </div>
                </div>

                <div class="social-auth-links text-center mb-2">
                    <button class="btn btn-block btn-primary swalDefaultSuccess"> Log-In</button>
                </div>
            <!-- </form> -->

            

            <p class="mb-1">
                <div class="col-12 text-right">
                    <a href="forgot-password.html">Lupa Password?</a>    
                </div>
                <div class="col-12 text-right">
                    
                </div>
                </div>
            </p>

            </div>
            <!-- /.login-card-body -->
        </div>
    </div>

    <!-- jQuery library -->
    <script src="{{asset('assets/jquery/jquery.min.js')}}"></script>
    <!-- Latest compiled JavaScript -->
    <script src="{{asset('assets/bootstrap\js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>
</body>
</html>
<script type="text/javascript">
    $(window).on('load', function() {
	    $("#splash").delay(2000).fadeOut("slow");
    })

    $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000
    });
    
    $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        type: 'success',
        title: 'Berhasil Login...'
      }),
      window.location.href='/operator-dashboard';
    });

  });
</script>
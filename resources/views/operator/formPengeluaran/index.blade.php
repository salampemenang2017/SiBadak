@extends('layouts.operator.master')

@section('rute', '/operator-dashboard')

@section('menu', 'Info Pengeluaran')

@section('title', 'Form-Input Pengeluaran')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i></span> Form Input Pengeluaran</h4>
			<div class="container">
				<p style="margin-bottom: -10px; margin-top: 10px;">Silahkan Anda Pilih Terlebih Dahulu Kegiatan Pengeluaran Dibawah Sini</p>
			</div>

			<hr style="border: 1px solid black;">

			<div class="container" style="margin-bottom: 15px;">
				<div class="col-sm-2 col-xs-2">
					<span class="text-danger"><i class="fa fa-warning fa-3x"></i></span>
				</div>
				<div class="col-sm-10 col-xs-10">
					<p> Link Akan Muncul Ketika Pengajuan Sudah Diverifikasi / Acc</p>
				</div>
			</div>
		</div>

		<div class="col-sm-10 col-sm-offset-1">
			<div class="alert alert-info">
				Pembangunan Toilet Sekolah 4 Pintu Dan Tempat Wudhu <a href="{{ route('pengeluaran1') }}"><i class="fa fa-external-link"></i></a>
			</div>
		</div>

		<div class="col-sm-10 col-sm-offset-1">
			<div class="alert alert-info">
				Pembangunan Ruang Kelas Baru Dengan Ukuran 40 X 40 Pondasi Anti Gempa <a href="{{ route('pengeluaran2') }}"><i class="fa fa-external-link"></i></a>
			</div>
		</div>

	</div>
</div>
@stop

@push('scripts')
<script>
	 $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    
    $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        type: 'success',
        title: 'Data Berhasil Disimpan'
      })
    });

  });
</script>
@endpush

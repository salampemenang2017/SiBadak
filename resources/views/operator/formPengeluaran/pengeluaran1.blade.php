@extends('layouts.operator.master')

@section('rute', '/input-pengeluaran')

@section('menu', 'Input Pengeluaran 1')

@section('title', 'Form-Input Pengeluaran')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 style="font-weight: bold; color: #3498db; font-size: 20px;"><span><i class="fa fa-info-circle"></i></span> Form Input Pengeluaran</h4>
			<div class="container">
				<p style="margin-bottom: -10px; margin-top: 10px;">Anda Dapat Mendata Pengeluaran Kegiatan Pekerjaan Setiap Harinya</p>
			</div>

			<hr style="border: 1px solid black;">
		</div>

		<div class="col-sm-10 col-sm-offset-1" style="margin-bottom: 30px;">
			<div class="alert alert-info" style="height: auto;">
				<span>Pembangunan Toilet Sekolah 4 Pintu Dan Tempat Wudhu</span>
			</div>
		</div>

		<div class="text-right" style="margin-right: 20px;">
			<a href="{{ route('operator-detail-rab1') }}" class="btn btn-sm btn-primary">Detailt RAB</a>
		</div>

		<form>
			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="dp">Deskripsi Pekerjaan</label>
					<input type="text" value="Pekerjaan Pengkuran" id="dp" class="form-control" onclick="onType()">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="vol">Vol.</label>
					<input type="text" value="1" id="vol" class="form-control" onclick="onType()">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="satuan">Satuan</label>
					<input type="text" value="Is" id="satuan" class="form-control" onclick="onType()">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="hs">Harga Satuan</label>
					<input type="text" value="150,000" id="hs" class="form-control" onclick="onType()">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="jh">Jumlah Harga</label>
					<input type="text" value="150,000" id="jh" class="form-control" readonly="">
				</div>
			</div>

			<div class="col-sm-10 col-sm-offset-1">
				<div class="form-group">
					<label for="proposal">Foto Faktur</label>
					<div class="row">
						<div class="col-md-10 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="Silahkan Pilih File..." readonly="" id="uploadFile">
							<input type="file" id="proposal" style="display: none;" accept=".png, .jpg, .jpeg, .pdf">
						</div>

						<div class="col-md-2 col-sm-4 col-xs-4">
							<label class="btn btn-primary" for="proposal">Browse</label>
						</div>
					</div>
				</div>
			</div>

			<br />

			<div class="col-sm-4 col-sm-offset-4 text-center">
				<button type="submit" class="btn btn-md btn-info swalDefaultSuccess" style="width: 200px;">Simpan</button>
			</div>
		</form>
	</div>
</div>

@section('id')
id="nav"
@stop

@stop

@push('scripts')
<script>
// Function UploadFile (Value)
document.getElementById('proposal').onchange = function () {
	document.getElementById('uploadFile').value = this.value;
}

// Function On Typing
function onType() {
	document.getElementById('nav').style.position = "relative";
}

// Swall-Alert
$(function() {
	const Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});

	$('.swalDefaultSuccess').click(function() {
		Toast.fire({
			type: 'success',
			title: 'Data Berhasil Disimpan'
		})
	});

});
</script>
@endpush

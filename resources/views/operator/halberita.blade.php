@extends('layouts.operator.master')

@section('rute', '/operator-dashboard')

@section('menu', 'Berita')

@section('title', 'Form-Input MasterSekolah')

@section('content')
<div class="container">
    <div class="row">
        <!-- Berita1 -->
        <div class="col-sm-12" style="margin-top: 20px;">
            <div class="alert"  style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s; border: 1px #fff;">
                <p style="font-size: 15px; font-weight: bold;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
                <span style="color: #ccc;">2020-01-01</span>

                <p style="color: #aaa; margin-top: 5px;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
                <img src="{{asset('img/berita/berita-1.jpg')}}" width="100%" alt="Gambar" class="rounded img-fluid">
                <br /><br />
            </div>
        </div>
        <!-- End Berita1 -->

          <!-- Berita2 -->
        <div class="col-sm-12">
            <div class="alert"  style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s; border: 1px #fff;">
                <p style="font-size: 15px; font-weight: bold;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
                <span style="color: #ccc;">2020-01-01</span>

                <p style="color: #aaa; margin-top: 5px;">Pemerintah Cianjur Gulirkan Program Bantuan Sekolah</p>
                <img src="{{asset('img/berita/berita-1.jpg')}}" width="100%" alt="Gambar" class="rounded img-fluid">
                <br /><br />
            </div>
        </div>
        <!-- End Berita2 -->
    </div>
</div>

@stop
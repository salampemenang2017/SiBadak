<!-- #F4F6F9 -->
@extends('layouts.operator.blank')

@section('title')
    Profile User
@stop

@section('content')

    <!-- Atas  -->
    <div class="kotak gradient1" style="height: auto;">
        <div class="container">
            <div class="col-sm-12" style="margin-top: 5px; margin-left: -10px;">
            <a href="{{ route('operator-dashboard') }}" style="color: #fff;"><i class="fa fa-angle-left fa-1x"></i> Kembali</a>
            </div>
            <div class="col-md-12" style="padding-bottom: 30px;">
                <div class="container">
                    <div class="col-sm-12 text-center" style="padding-top: 25px;">
                    <img src="{{ asset('/assets/fixHealt/img/smaknis.png') }}" alt="" width="50%">
                    </div>

                    <div class="col-sm-12 text-center">
                        <p style="color: #fff; font-size: 25px; margin-top: 10px; margin-bottom: -2px; font-weigt: bold;">SMK Nurul Islam</p>
                        <p style="color: #fff; font-weight: bold; margin-left-50px;">Operator</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Bawah -->
    <div>
        <div style="width: 100%; height: auto; min-height: 500px; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s; border: 1px #fff;">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12 sol-xs-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab" style="margin-right: 110px;">Info Pengajuan</a></li>
                            <li role="presentation"><a href="#update" aria-controls="update" role="tab" data-toggle="tab">Edit Profile</a></li>
                        </ul>
                        <!-- End Nav Tabs -->
                    </div>
            
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start info-pengajuan -->
                        <div role="tabpanel" class="tab-pane  active" id="info" style="margin-top: 30px;">
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                <strong>Data Pengajuan Kegiatan</strong>
                            </div>

                            <!-- Start Total -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="alert " style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                                transition: 0.3s; border: 1px #fff;" >
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-2">
                                            <span style="color: #6C757D;"><i class="fa fa-credit-card fa-3x"></i></span> 
                                        </div>
                                        <div class="col-sm-10 col-xs-10">
                                            <strong>Total Anggaran Yang Diterima</strong> <br />
                                            <span>Rp. 35.000.000,00-</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Total -->

                            <!-- Start Terima -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="alert " style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                                transition: 0.3s; border: 1px #fff;" >
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-2">
                                            <span class="text-success"><i class="fa fa-check fa-3x"></i></span> 
                                        </div>
                                        <div class="col-sm-10 col-xs-10">
                                            <strong>Pengajuan Diterima</strong> <br />
                                            <span>1</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Terima -->

                            <!-- Start Tolak -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="alert " style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                                transition: 0.3s; border: 1px #fff;" >
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-2">
                                            <span class="text-danger"><i class="fa fa-times fa-3x"></i></span> 
                                        </div>
                                        <div class="col-sm-10 col-xs-10">
                                            <strong>Pengajuan Ditolak</strong> <br />
                                            <span>0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Tolak -->

                            <!-- Start Anggaran -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="alert " style="height: auto; width: 100%; background-color: #fff; padding-top: 15px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                                transition: 0.3s; border: 1px #fff;" >
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-2">
                                            <span class="text-primary"><i class="fa fa-book fa-3x"></i></span> 
                                        </div>
                                        <div class="col-sm-10 col-xs-10">
                                            <strong>Total Pengajuan</strong> <br />
                                            <span>1</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Anggaran -->
                        </div>
                        <!-- End info-pengajuan -->

                         <!-- Start EditProfile -->
                         <div role="tabpanel" class="tab-pane" id="update" style="margin-top: 30px;">
                            <form action="#">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="form-group">
                                        <label for="ns">Nama Sekolah</label>
                                        <input type="text" name="namaSekolah" id="ns" class="form-control" value="SMK Nurul Islam" onclick="onType()">
                                    </div>
                                </div>
                    
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <label for="jsti">Jml.Siswa.Thn.ini</label>
                                                <input type="number" name="jsti" id="jsti" class="form-control" value="1500" onclick="onType()">
                                            </div>
                                        </div>
                    
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <label for="jsts">Jml.Siswa.Thn.Sblm</label>
                                                <input type="number" name="jsts" id="jsts" class="form-control" value="2000" onclick="onType()">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    
                                <br />
                    
                                <div class="col-sm-4 col-sm-offset-4 text-center">
                                    <button type="submit" class="btn btn-md btn-info swalDefaultSuccess" style="width: 200px;">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <!-- End EditProfile -->
                    </div>
                    <!-- End Tab-panes -->
                </div>
            </div>

            <br /><br /><br />
        </div>
    </div>

    <!-- info -->
    {{-- <div class="container" style="margin-top: 25px;">
        <div class="row">
            <div class="col-sm-3 col-xs-3">
                <div style="width: 100%; height: auto; padding: 2px; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s; border: 1px #fff;">
                    <div class="text-center">
                       <span style="font-size: 25px;"><i class="fa fa-book"></i></span>                          
                    </div>
                </div>
            </div>

             <div class="col-sm-3 col-xs-3">
                <div style="width: 100%; height: auto; padding: 2px; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s; border: 1px #fff;">
                    <div class="text-center">
                       <span style="font-size: 25px;"><i class="fa fa-book"></i></span>                          
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-xs-3">
                <div style="width: 100%; height: auto; padding: 2px; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s; border: 1px #fff;">
                    <div class="text-center">
                       <span style="font-size: 25px;"><i class="fa fa-book"></i></span>                          
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-xs-3">
                <div style="width: 100%; height: auto; padding: 2px; background-color: #fff; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s; border: 1px #fff;">
                    <div class="text-center">
                       <span style="font-size: 25px;"><i class="fa fa-book"></i></span>                          
                    </div>
                </div>
            </div>
        </div>
    </div> --}} 
    
@stop

@push('scripts')
<script>
    // Swall-Alert
	$(function() {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		$('.swalDefaultSuccess').click(function() {
			Toast.fire({
				type: 'success',
				title: 'Data Berhasil Diubah'
			})
		});

	});
</script>
@endpush


  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link href="{{asset('/assets/font-awesome/css/all.min.css')}}" rel="stylesheet" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('/assets/dist/css/adminlte.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('/assets/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    {{-- Custom CSS --}}
    @yield('style-css')

    <link rel="stylesheet" href="{{asset('/assets/datatables-bs4/css/dataTables.bootstrap4.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- jQuery -->
    <script src="{{ asset('/assets/jquery/jquery.min.js') }}"></script> 
    @yield('api-maps')
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" id="Cmenu" onclick="miniLogo()" data-widget="pushmenu" href="#">
              <i class="fas fa-bars"></i>
            </a>
            <a style="display: none;" class="nav-link" id="Smenu" onclick="BigLogo()" data-widget="pushmenu" href="#">
              <i class="fas fa-bars"></i>
            </a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">@yield('page-name')</a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <!-- Sidebar -->
        <div class="sidebar mt-2" >
          <div class="">
            <div class="container-fluid">
              <img src="/img/logo/Logoo.png" id="logo" class="logoSBmini" style="display: none;">
              <img src="/img/logo/Logoo.png" id="Blogo" class="logoSB">
              <br>
            </div>
            <br>
            <center><strong style="color: #D6D8D9;" class="text-white">Menu</strong></center>
            <!-- Sidebar user panel (optional) -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                  <a href="{{url('/papan-informasi')}}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt  text-white"></i>
                    <p class=" text-white">
                      Papan informasi
                    </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('dinas-maps-dak') }}" class="nav-link">
                    <i class="nav-icon fas fa-map-marked-alt  text-white"></i>
                    <p class=" text-white">
                      Peta DAK Sekolah
                    </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('daftar-pengajuan-dak')}}" class="nav-link">
                    <i class="nav-icon fas fa-list  text-white"></i>
                    <p class=" text-white">
                     Daftar Pengajuan DAK
                   </p>
                 </a>
               </li>
             </ul>

             <!-- Sidebar Menu -->
             <nav class="mt-2">
              @yield('sidecontent')
            </nav>
          </nav>
        </div>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <strong>Design By .......</strong>
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
      </div>
    </footer>
  </div>
  <!-- ./wrapper -->


  <!-- Bootstrap 4 -->
  <script src="{{asset('/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  
  <script src="{{asset('/assets/flot/jquery.flot.js')}}"></script>
  <!-- ChartJS -->
  <script src="{{asset('/assets/chart.js/Chart.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{asset('/assets/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <!-- overlayScrollbars -->
  <script src="{{asset('/assets/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('/assets/dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('/assets/dist/js/demo.js')}}"></script>

  <script src="{{asset('/assets/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/assets/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

  <script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>

  <script type="text/javascript">
    function miniLogo() {
      document.getElementById("logo").style.display = "block";
      document.getElementById("Blogo").style.display = "none";
      document.getElementById("Cmenu").style.display = "none";
      document.getElementById("Smenu").style.display = "block";
    }
    function BigLogo() {
      document.getElementById("logo").style.display = "none";
      document.getElementById("Blogo").style.display = "block";
      document.getElementById("Cmenu").style.display = "block";
      document.getElementById("Smenu").style.display = "none";
    }
  </script>

  @yield('script-js')

</body>
</html>

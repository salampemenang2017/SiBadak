<!-- Start Naviagasi -->
<div class="phone-box wrap push">
	<div class="menu-notify navbar-fixed-top">
		<a href="@yield('rute')" style="text-decoration: none;">
			<span style="margin-left: 20px; font-size: 20px; color: #fff;"><i class="fa fa-angle-left"></i></span>
			<span style="margin-left: 10px; font-size: 20px; color: #fff;">@yield('menu')</span>
		</a>
		{{-- <div class="profile-left">
			<a href="#">
				<i class="fa fa-th-large"></i>
			</a>
		</div>
		<div class="Profile-mid">
			<h5 class="pro-link font">SiBadak</h5>
		</div>
		<div class="Profile-right">
			<div id="dd1" class="wrapper-dropdown-1" tabindex="1">
				<i class="fa fa-info-circle"></i>
				<ul class="dropdown">
					<li><a href="#">Bantuan</a></li>
					<li><a href="#">Tentang Aplikasi</a></li>
				</ul>
			</div>
		</div> --}}
		<div class="clearfix"></div>
	</div> 
</div>
<!-- End Navigasi -->

<script type="text/javascript">
	function DropDown(el) {
		this.dd = el;
		this.initEvents();
	}
	DropDown.prototype = {
		initEvents : function() {
			var obj = this;

			obj.dd.on('click', function(event){
				$(this).toggleClass('active');
				event.stopPropagation();
			});	
		}
	}
	$(function() {

		var dd = new DropDown( $('#dd1') );

		$(document).click(function() {
			// all dropdowns
			$('.wrapper-dropdown-1').removeClass('active');
		});

	});
</script>

<br /><br />
<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>SiBadak | @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--font-awsome-css-->
	<link rel="stylesheet" href="{{asset('assets/fixHealt/css/font-awesome.min.css')}}"> 
	<!--bootstrap-->
	<link href="{{asset('assets/fixHealt/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<!--custom css-->
	<link href="{{asset('assets/fixHealt/css/style.css')}}" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{asset('assets/fixHealt/css/percircle.css')}}">
	<!-- Swal CSS -->
	<link rel="stylesheet" href="{{asset('/assets/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
	<!--component-css-->
	<script src="{{asset('assets/jquery/jquery.min.js')}}"></script>
	<script src="{{asset('assets/fixHealt/js/bootstrap.min.js')}}"></script>
	<!-- Swal JS -->
	<script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>
</head>
<body>

	<div class="body-pack">
		<div class="masthead pdng-stn1">
			
			<!-- Start Navbar -->
			@include('layouts.operator.partials.navbar')
			<!-- End Navbar -->

			<!-- Start Content -->
			<div class="parker">
				@yield('content')
			</div>
			<!-- End Content -->

			<br /><br /><br /><br /><br />

			<!-- Start Quick-Menu -->
			{{-- <div class="like-icons text-center" style="position: fixed; left: 0; bottom: 0; width: 100%" @yield('id')>
				<ul>
					<li>
						<a href="{{ route('operator-dashboard') }}"><i class="fa fa-th"></i></a>
						<h6>Beranda</h6>
					</li>
					<li>
						<a href="{{ route('form-masterSekolah') }}"><i class="fa fa-graduation-cap"></i></a>
						<h6>Sekolah</h6>
					</li>
					<li>
						<a href="{{ route('dak-master') }}"><i class="fa fa-cloud-upload"></i></a>
						<h6>Pengajuan</h6>
					</li>
					<li>
						<a href="{{ route('form-pengeluaran') }}"><i class="fa fa-credit-card"></i></a>
						<h6>Pengeluaran</h6>
					</li>
				</ul>
			</div>				 --}}
			<!-- End Quick-Menu -->

		</div>
	</div>

	@stack('scripts')

	
</body>
</html>
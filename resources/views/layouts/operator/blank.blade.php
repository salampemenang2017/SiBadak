<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<title>SiBadak | @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--font-awsome-css-->
	<link rel="stylesheet" href="{{asset('assets/fixHealt/css/font-awesome.min.css')}}"> 
	<!--bootstrap-->
	<link href="{{asset('assets/fixHealt/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<!--custom css-->
	<link href="{{asset('assets/fixHealt/css/style.css')}}" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{asset('assets/fixHealt/css/percircle.css')}}">
	<!-- Swal CSS -->
	<link rel="stylesheet" href="{{asset('/assets/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
	<!--component-css-->
	<script src="{{asset('assets/jquery/jquery.min.js')}}"></script>
	<script src="{{asset('assets/fixHealt/js/bootstrap.min.js')}}"></script>
	<!-- Swal JS -->
	<script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>
</head>
<body class="background-color: #fff;">

	<div class="body-pack">
		<div class="masthead pdng-stn1">
			<!-- Start Content -->
				@yield('content')
			<!-- End Content -->
		</div>
	</div>
	
	@stack('scripts')
	
</body>
</html>
<style>
	
	.gradient1 {
	height: 250px;
	/* #FCAF01 Orange */ /* #0F466E #00ace9 #45aaf2  Biru*/
	background: linear-gradient(#45aaf2 0%, #0074d9 100%);
	/* Standard syntax */
	
	background: -webkit-linear-gradient(#45aaf2 0%, #0074d9 100%);
	/* For Safari 5.1 to 6.0 */
	
	background: -o-linear-gradient(#45aaf2 0%, #0074d9 100%);
	/* For Opera 11.1 to 12.0 */
	
	background: -moz-linear-gradient(#45aaf2 0%, #0074d9 100%);
	/* For Firefox 3.6 to 15 */
	}
</style>
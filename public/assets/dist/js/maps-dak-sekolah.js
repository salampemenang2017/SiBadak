function initMap() {
  var x = '/img/logo/x.gif';
  var y = '/img/logo/map.gif';
  var ico = '/img/logo/marker.png';
  var nuris = {
    info:
    '<div class="row p-3">\
    <div class="col-sm-3">\
    <img src="/assets/dist/img/nuris.png" width="80" height="80">\
    </div>\
    <div class="col-sm-9">\
    <h6>SMK Nurul Islam Cianjur - Pencairan DAK</h6><br>\
    <strong class="text-dark">IDR.</strong> 1.425.000,- \
    <a href="/riwayat-daftar-kegiatan-dak" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
    \
    </div>\
    </div>',
    lat: -6.804109,
    long: 107.219391,
  }

  var smkibro = {
    info:
    '<div class="row">\
    <div class="col-sm-3">\
    <img src="/img/logo/peringatan.png" width="100" height="100">\
    </div>\
    <div class="col-sm-9">\
    <h4>SMK Al-Ibrohimiyah</h4><br>\
    <strong class="text-danger">Peringatan!</strong> Sekolah tersebut belum menerima DAK sejak tahun 2017\
    </div>\
    </div>',
    lat: -6.811387, 
    long: 107.232377,
  }

  var smpn2sukaluyu = {
    info:
    '<div class="row p-3">\
    <div class="col-sm-3">\
    <img src="/img/logo/default.jpg" width="80" height="80">\
    </div>\
    <div class="col-sm-9">\
    <h6>SMPN 2 Sukaluyu Cianjur - Pencairan DAK</h6><br>\
    <strong class="text-dark">IDR.</strong>  35,000,000.- \
    <a href="/riwayat-daftar-kegiatan-dak/smpn-2-sukaluyu" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
    \
    </div>\
    </div>',
    lat: -6.816531, 
    long: 107.234277,
  }

  var smpn1karangtengah = {
    info:
    '<div class="row p-3">\
    <div class="col-sm-3">\
    <img src="/img/logo/smpn-1-karangtengah.png" width="80" height="80">\
    </div>\
    <div class="col-sm-9">\
    <h6>SMPN 1 Karang Tengah - Pencairan DAK</h6><br>\
    <strong class="text-dark">IDR.</strong>  35,000,000.- \
    <a href="/riwayat-daftar-kegiatan-dak/smp-karangtengah-1" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
    \
    </div>\
    </div>',
    lat: -6.798307, 
    long: 107.187875,
  }

  var smpn1ciranjang = {
    info:
    '<div class="row p-3">\
    <div class="col-sm-3">\
    <img src="/img/logo/smp-1-crj.png" width="80" height="80">\
    </div>\
    <div class="col-sm-9">\
    <h6>SMPN 1 Ciranjang - Pencairan DAK</h6><br>\
    <strong class="text-dark">IDR.</strong>  35,000,000.- \
    <a href="/riwayat-daftar-kegiatan-dak/smp-1-ciranjang" class="float-right"><i class="fas fa-arrow-alt-circle-right text-success" title="Lihat Riwayat DAK"></i></a>\
    \
    </div>\
    </div>',
    lat: -6.813005, 
    long: 107.260186,
  }

  var lokasi_terverifikasi = [
  [nuris.info, nuris.lat, nuris.long, 0],
  [smpn2sukaluyu.info, smpn2sukaluyu.lat, smpn2sukaluyu.long, 0],
  [smpn1karangtengah.info, smpn1karangtengah.lat,smpn1karangtengah.long, 0],
  [smpn1ciranjang.info, smpn1ciranjang.lat, smpn1ciranjang.long, 0]
  ]

  var lokasi_ditolak = [
  [smkibro.info, smkibro.lat, smkibro.long, 0]
  ]

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: new google.maps.LatLng(-6.8030803, 107.2370874),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  })

  var infowindow = new google.maps.InfoWindow({})

  var marker, i
  var markers = [];

  for (i = 0; i < lokasi_terverifikasi.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(lokasi_terverifikasi[i][1], lokasi_terverifikasi[i][2]),
      map: map,
      icon: y,
    })

    google.maps.event.addListener(
      marker,
      'click',
      (function(marker, i) {
        return function() {
          infowindow.setContent(lokasi_terverifikasi[i][0])
          infowindow.open(map, marker)
        }
      })(marker, i)
      )
  }

  for (i = 0; i < lokasi_ditolak.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(lokasi_ditolak[i][1], lokasi_ditolak[i][2]),
      map: map,
      icon: x,
    })

    google.maps.event.addListener(
      marker,
      'click',
      (function(marker, i) {
        return function() {
          infowindow.setContent(lokasi_ditolak[i][0])
          infowindow.open(map, marker)
        }
      })(marker, i)
      )
  }
  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
       console.log("Returned place contains no geometry");
       return;
     }
     var icon = {
      url: y,
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
    };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
    map.fitBounds(bounds);
  });
}


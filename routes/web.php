<?php

// Start Link Dinas
Route::get('/papan-informasi', function () {
	return view('dinas.papan-informasi');
});

// Maps Papan Informasi
	Route::get('/papan-informasi/KabCianjur', function() {
		return view('dinas.maps-papan-informasi.cianjur');
	})->name('cianjur');
	Route::get('/papan-informasi/Agrabinta', function() {
		return view('dinas.maps-papan-informasi.agrabinta');
	})->name('agrabinta');
	Route::get('/papan-informasi/BojongPicung', function() {
		return view('dinas.maps-papan-informasi.bojongpicung');
	})->name('bojongpicung');
	Route::get('/papan-informasi/Campaka', function() {
		return view('dinas.maps-papan-informasi.campaka');
	})->name('campaka');
	Route::get('/papan-informasi/Kec-Cianjur', function() {
		return view('dinas.maps-papan-informasi.kec-cianjur');
	})->name('kec-cianjur');
	Route::get('/papan-informasi/CampakaMulya', function() {
		return view('dinas.maps-papan-informasi.campakamulya');
	})->name('campakamulya');
	Route::get('/papan-informasi/Cibeber', function() {
		return view('dinas.maps-papan-informasi.cibeber');
	})->name('cibeber');
	Route::get('/papan-informasi/Cibinong', function() {
		return view('dinas.maps-papan-informasi.cibinong');
	})->name('cibinong');
	Route::get('/papan-informasi/Cidaun', function() {
		return view('dinas.maps-papan-informasi.cidaun');
	})->name('cidaun');
	Route::get('/papan-informasi/cijati', function() {
		return view('dinas.maps-papan-informasi.cijati');
	})->name('cijati');
	Route::get('/papan-informasi/cikadu', function() {
		return view('dinas.maps-papan-informasi.cikadu');
	})->name('cikadu');
	Route::get('/papan-informasi/cikalongkulon', function() {
		return view('dinas.maps-papan-informasi.cikalongkulon');
	})->name('cikalongkulon');
	Route::get('/papan-informasi/cilaku', function() {
		return view('dinas.maps-papan-informasi.cilaku');
	})->name('cilaku');
	Route::get('/papan-informasi/cipanas', function() {
		return view('dinas.maps-papan-informasi.cipanas');
	})->name('cipanas');
	Route::get('/papan-informasi/ciranjang', function() {
		return view('dinas.maps-papan-informasi.ciranjang');
	})->name('ciranjang');
	Route::get('/papan-informasi/cugenang', function() {
		return view('dinas.maps-papan-informasi.cugenang');
	})->name('cugenang');
	Route::get('/papan-informasi/gekbrong', function() {
		return view('dinas.maps-papan-informasi.gekbrong');
	})->name('gekbrong');
	Route::get('/papan-informasi/haurwangi', function() {
		return view('dinas.maps-papan-informasi.haurwangi');
	})->name('haurwangi');
	Route::get('/papan-informasi/kadupandak', function() {
		return view('dinas.maps-papan-informasi.kadupandak');
	})->name('kadupandak');
	Route::get('/papan-informasi/karangtengah', function() {
		return view('dinas.maps-papan-informasi.karangtengah');
	})->name('karangtengah');
	Route::get('/papan-informasi/leles', function() {
		return view('dinas.maps-papan-informasi.leles');
	})->name('leles');
	Route::get('/papan-informasi/mande', function() {
		return view('dinas.maps-papan-informasi.mande');
	})->name('mande');
	Route::get('/papan-informasi/naringgul', function() {
		return view('dinas.maps-papan-informasi.naringgul');
	})->name('naringgul');
	Route::get('/papan-informasi/pacet', function() {
		return view('dinas.maps-papan-informasi.pacet');
	})->name('pacet');
	Route::get('/papan-informasi/pagelaran', function() {
		return view('dinas.maps-papan-informasi.pagelaran');
	})->name('pagelaran');
	Route::get('/papan-informasi/pasirkuda', function() {
		return view('dinas.maps-papan-informasi.pasirkuda');
	})->name('pasirkuda');
	Route::get('/papan-informasi/sindangbarang', function() {
		return view('dinas.maps-papan-informasi.sindangbarang');
	})->name('sindangbarang');
	Route::get('/papan-informasi/sukaluyu', function() {
		return view('dinas.maps-papan-informasi.sukaluyu');
	})->name('sukaluyu');
	Route::get('/papan-informasi/sukanagara', function() {
		return view('dinas.maps-papan-informasi.sukanagara');
	})->name('sukanagara');
	Route::get('/papan-informasi/sukaresmi', function() {
		return view('dinas.maps-papan-informasi.sukaresmi');
	})->name('sukaresmi');
	Route::get('/papan-informasi/takokak', function() {
		return view('dinas.maps-papan-informasi.takokak');
	})->name('takokak');
	Route::get('/papan-informasi/tanggeung', function() {
		return view('dinas.maps-papan-informasi.tanggeung');
	})->name('tanggeung');
	Route::get('/papan-informasi/warungkondang', function() {
		return view('dinas.maps-papan-informasi.warungkondang');
	})->name('warungkondang');
// End Maps Papan Informasi

Route::get('/daftar-pengajuan-dak', function () {
	return view('dinas.Daftar-Pengajuan-Dak');
});

Route::get('/maps-dak-sekolah', function() {
	return view('dinas.maps-dak-sekolah');
})->name('dinas-maps-dak');


Route::get('/riwayat-daftar-kegiatan-dak', function() {
	return view('dinas.riwayat-daftar-kegiatan-dak');
})->name('dinas-riwayat-dak');

Route::get('laporan-rancangan-anggaran-biaya-sekolah', function() {
	return view('dinas.laporan-rab-sekolah');
})->name('dinas-laporan-rab');

// Maps Alamat Sekolah
Route::get('peta/SMKNurulIslamCianjur', function() {
	return view('dinas.maps_sekolah.smk_nuris');
})->name('maps-nuris');
Route::get('peta/SMK-Al-Ibrohimiyah', function() {
	return view('dinas.maps_sekolah.smk_ibro');
})->name('maps-bpc');
Route::get('peta/SMPN2Sukaluyu', function() {
	return view('dinas.maps_sekolah.smp_dua_sukaluyu');
})->name('maps-smp_dua_sukaluyu');
Route::get('peta/SMPN1KarangTengah', function() {
	return view('dinas.maps_sekolah.smpn_karang_tengah_satu');
})->name('maps-karatsu');
Route::get('peta/SMPN1Ciranjang', function() {
	return view('dinas.maps_sekolah.smpn_satu_ciranjang');
})->name('maps-andir');
//riwayat sekolah
Route::get('riwayat-daftar-kegiatan-dak/smp-karangtengah-1', function() {
	return view('dinas.riwayat-sekolah.riwayat-daftar-kegiatan-smp-karangtengah-1');
});
Route::get('laporan-rancangan-anggaran-biaya-sekolah/smp-karangtengah-1', function() {
	return view('dinas.riwayat-sekolah.laporan-rab-smp-karangtengah-1');
});
Route::get('riwayat-daftar-kegiatan-dak/smp-1-ciranjang', function() {
	return view('dinas.riwayat-sekolah.riwayat-daftar-kegiatan-smp-1-ciranjang');
});
Route::get('laporan-rancangan-anggaran-biaya-sekolah/smp-1-ciranjang', function() {
	return view('dinas.riwayat-sekolah.laporan-rab-smp-1-ciranjang');
});
Route::get('riwayat-daftar-kegiatan-dak/smpn-2-sukaluyu', function() {
	return view('dinas.riwayat-sekolah.riwayat-daftar-kegiatan-smpn-2-sukaluyu');
});
Route::get('laporan-rancangan-anggaran-biaya-sekolah/smpn-2-sukaluyu', function() {
	return view('dinas.riwayat-sekolah.laporan-rab-smpn-2-sukaluyu');
});


// Start Link Operator
Route::get('/', function () {
    return view('operator.welcome');
})->name('operator-login');

Route::get('/operator-dashboard', function () {
    return view('operator.dashboard');
})->name('operator-dashboard');

Route::get('/operator-profile', function () {
    return view('operator.profile');
})->name('operator-profile');

// Operator : Detail-Rab
Route::get('/detail-rab1', function(){
    return view('operator.detailRab.detailRab1');
})->name('operator-detail-rab1');

Route::get('/detail-rab2', function(){
    return view('operator.detailRab.detailRab2');
})->name('operator-detail-rab2');

// Operator : Form-Input
Route::get('/input-pengeluaran', function () {
	return view('operator.formPengeluaran.index');
})->name('form-pengeluaran');

Route::get('/pengeluaran1', function () {
	return view('operator.formPengeluaran.pengeluaran1');
})->name('pengeluaran1');

Route::get('/pengeluaran2', function () {
	return view('operator.formPengeluaran.pengeluaran2');
})->name('pengeluaran2');

Route::get('/input-masterSekolah', function () {
	return view('operator.formSekolah');
})->name('form-masterSekolah');

Route::get('/dak-master', function () {
	return view('operator.dakMaster');
})->name('dak-master');

// Operator : Upload-Foto
Route::get('/upload-sebelum', function () {
	return view('operator.upload.sebelum');
})->name('uploadB');

Route::get('/upload-proses', function () {
	return view('operator.upload.proses');
})->name('uploadP');

Route::get('/upload-selesai', function () {
	return view('operator.upload.selesai');
})->name('uploadE');

// Berita
Route::get('/operator-berita', function(){
	return view('operator.berita');
})->name('operator-berita');

Route::get('/operator-berita/halaman', function(){
	return view('operator.halberita');
})->name('operator-halberita');


